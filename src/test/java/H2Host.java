import org.h2.tools.Server;

import java.sql.SQLException;

public class H2Host {
    public static void main(String... args) {
        try {
            Server.createTcpServer().start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
