package com.spiderbiggen.steampricetracker.util;

import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.ui.StackPaneTransition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.spiderbiggen.steampricetracker.ui.StackPaneTransition.TransitionType.FADE;
import static com.spiderbiggen.steampricetracker.ui.StackPaneTransition.TransitionType.TRANSLATE;
import static java.time.temporal.ChronoUnit.WEEKS;
import static org.junit.jupiter.api.Assertions.*;

public class SettingsTest {

    private Random random;

    @BeforeEach
    public void setUp() throws IOException {
        Settings.setSaveFile("test_save.properties");
        Settings.loadDefaults();
        random = new Random();
    }

    @AfterEach
    public void tearDown() {
        Settings.clearProperties();
        Settings.resetSaveFile();
    }

    private String getAppId() {
        return String.valueOf(random.nextInt(10000));
    }

    private String getRandomString() {
        return UUID.randomUUID().toString();
    }

    @Test
    public void getItems() {
        Set<Item> expected = new HashSet<>();
        Set<Item> items = Settings.getItems();
        assertEquals(expected, items);
    }

    @Test
    public void setItems() {
        Set<Item> expected = new HashSet<>(Arrays.asList(
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString())
        ));
        Settings.setItems(expected);
        Set<Item> items = Settings.getItems();
        assertEquals(expected, items);
        assertTrue(Settings.isChanged());
    }

    @Test
    public void getAppids() {
        List<Item> items = Arrays.asList(
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString())
        );
        Set<String> expected = items.stream().map(Item::getAppid).collect(Collectors.toSet());
        Settings.setItems(items);
        Set<String> appids = Settings.getAppids();
        assertEquals(expected, appids);
    }

    @Test
    public void getAppidKeys() {
        List<Item> items = Arrays.asList(
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString())
        );
        Set<String> expected = new HashSet<>(Arrays.asList("APP_ID0", "APP_ID1", "APP_ID2", "APP_ID3"));
        Settings.setItems(items);
        Set<String> appidKeys = Settings.getAppidKeys();
        assertEquals(expected, appidKeys);
    }

    @Test
    public void getToken() {
        assertNull(Settings.getToken());
    }

    @Test
    public void setToken() {
        String token = getRandomString();
        Settings.setToken(token);
        assertEquals(token, Settings.getToken());
        assertTrue(Settings.isChanged());
    }

    @Test
    public void hasCurrency() {
        assertFalse(Settings.hasCurrency());
    }

    @Test
    public void getCurrency() {
        assertThrows(NullPointerException.class, Settings::getCurrency);
    }

    @Test
    public void setCurrency() {
        Currency currency = Currency.EUR;
        Settings.setCurrency(currency);
        assertEquals(currency, Settings.getCurrency());
        assertTrue(Settings.isChanged());
    }

    @Test
    public void getTransitionType() {
        assertEquals(TRANSLATE, Settings.getTransitionType());
    }

    @Test
    public void setTransitionType() {
        Settings.setTransitionType(FADE);
        assertEquals(FADE, Settings.getTransitionType());
        assertTrue(Settings.isChanged());
    }

    @Test
    public void getIntervalLength() {
        assertEquals(2, Settings.getIntervalLength());
    }

    @Test
    public void setIntervalLength() {
        int interval = random.nextInt(10);
        Settings.setIntervalLength(interval);
        assertEquals(interval, Settings.getIntervalLength());
        assertTrue(Settings.isChanged());
    }

    @Test
    public void getIntervalUnit() {
        assertEquals(WEEKS, Settings.getIntervalUnit());
    }

    @Test
    public void setIntervalUnit() {
        ChronoUnit expected = ChronoUnit.MONTHS;
        Settings.setIntervalUnit(expected);
        assertEquals(expected, Settings.getIntervalUnit());
        assertTrue(Settings.isChanged());
    }

    @Test
    public void loadDefaults() throws IOException {
        Settings.loadDefaults();
        assertNull(Settings.getToken());
        assertThrows(NullPointerException.class, Settings::getCurrency);
        assertEquals(new HashSet<>(), Settings.getAppidKeys());
        assertEquals(new HashSet<>(), Settings.getAppids());
        assertEquals(new HashSet<>(), Settings.getItems());
        assertEquals(2, Settings.getIntervalLength());
        assertEquals(WEEKS, Settings.getIntervalUnit());
        assertEquals(TRANSLATE, Settings.getTransitionType());
    }

    @Test
    public void saveProperties() throws IOException {
        ChronoUnit seconds = ChronoUnit.SECONDS;
        int interval = random.nextInt(10);
        StackPaneTransition.TransitionType transitionType = FADE;
        String token = getRandomString();
        Currency currency = Currency.JPY;
        Set<Item> expected = new HashSet<>(Arrays.asList(
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString()),
                new Item(getAppId(), getRandomString())
        ));
        Set<String> appids = expected.stream().map(Item::getAppid).collect(Collectors.toSet());
        Set<String> appidKeys = new HashSet<>(Arrays.asList("APP_ID0", "APP_ID1", "APP_ID2", "APP_ID3"));

        Settings.setIntervalUnit(seconds);
        Settings.setIntervalLength(interval);
        Settings.setTransitionType(transitionType);
        Settings.setToken(token);
        Settings.setCurrency(currency);
        Settings.setItems(expected);
        Settings.saveProperties();
        File file = new File(Settings.getSaveFile());
        assertTrue(file.exists());
        Settings.setupProperties();

        assertEquals(seconds, Settings.getIntervalUnit());
        assertEquals(interval, Settings.getIntervalLength());
        assertEquals(transitionType, Settings.getTransitionType());
        assertEquals(token, Settings.getToken());
        assertEquals(currency, Settings.getCurrency());
        Set<Item> items = Settings.getItems();
        assertEquals(expected, items);
        assertEquals(appids, Settings.getAppids());
        assertEquals(appidKeys, Settings.getAppidKeys());
    }

    @Test
    public void clearProperties() {
    }

    @Test
    public void resetSaveFile() {
    }

    @Test
    public void getSaveFile() {
    }

    @Test
    public void setSaveFile() {
    }
}