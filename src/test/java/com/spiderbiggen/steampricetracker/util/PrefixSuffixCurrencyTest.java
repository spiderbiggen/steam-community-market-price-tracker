package com.spiderbiggen.steampricetracker.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrefixSuffixCurrencyTest {

    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"$", "", Currency.USD},
                {"\u00a3", "", Currency.GBP},
                {"", "\u20ac", Currency.EUR},
                {"CHF ", "", Currency.CHF},
                {"", " p\u0443\u0431.", Currency.RUB},
                {"R$ ", "", Currency.BRL},
                {"\u00a5 ", "", Currency.JPY},
                {"", " kr", Currency.SEK},
                {"Rp 17 ", "", Currency.IDR},
                {"MR", "", Currency.MYR},
                {"P", "", Currency.PHP},
                {"S$", "", Currency.SGD},
                {"\u0e3f", "", Currency.THB},
                {"\u20a9 ", "", Currency.KRW},
                {"", " TL", Currency.TRY},
                {"Mex$ ", "", Currency.MXN},
                {"CDN$ ", "", Currency.CAD},
                {"NZ$ ", "", Currency.NZD},
                {"\u00a5 ", "", Currency.JPY},
                {"\u20b9 ", "", Currency.INR},
                {"CLP$ ", "", Currency.CLP},
                {"S/ ", "", Currency.PEN},
                {"COL$ ", "", Currency.COP},
                {"R ", "", Currency.ZAR},
                {"HK$ ", "", Currency.HKD},
                {"NT$ ", "", Currency.TWD},
                {"", " SR", Currency.SAR},
                {"", " AED", Currency.AED}
        });
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testFromPrefixAndSuffix(String prefix, String suffix, Currency result) {
        assertEquals(result, Currency.fromPrefixAndSuffix(prefix, suffix));
    }
}