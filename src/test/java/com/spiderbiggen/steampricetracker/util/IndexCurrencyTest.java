package com.spiderbiggen.steampricetracker.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IndexCurrencyTest {

    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Currency.USD, 1},
                {Currency.GBP, 2},
                {Currency.EUR, 3},
                {Currency.CHF, 4},
                {Currency.RUB, 5},
                {Currency.BRL, 7},
                {Currency.JPY, 8},
                {Currency.SEK, 9},
                {Currency.IDR, 10},
                {Currency.MYR, 11},
                {Currency.PHP, 12},
                {Currency.SGD, 13},
                {Currency.THB, 14},
                {Currency.KRW, 16},
                {Currency.TRY, 17},
                {Currency.MXN, 19},
                {Currency.CAD, 20},
                {Currency.NZD, 22},
                {Currency.CNY, 23},
                {Currency.INR, 24},
                {Currency.CLP, 25},
                {Currency.PEN, 26},
                {Currency.COP, 27},
                {Currency.ZAR, 28},
                {Currency.HKD, 29},
                {Currency.TWD, 30},
                {Currency.SAR, 31},
                {Currency.AED, 32}
        });
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testValueSring(Currency currency, int result) {
        assertEquals(result, currency.getIndex());
    }
}