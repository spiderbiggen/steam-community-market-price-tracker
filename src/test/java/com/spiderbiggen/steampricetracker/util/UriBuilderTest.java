package com.spiderbiggen.steampricetracker.util;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UriBuilderTest {

    private UriBuilder builder;

    @BeforeEach
    public void setUp() throws Exception {
        builder = new UriBuilder();
    }

    @Test
    public void simpleAddress() throws URISyntaxException {
        builder = new UriBuilder("http", "spiderbiggen.com");
        assertEquals("http://spiderbiggen.com", builder.buildString());
    }

    @Test
    public void authorityOnly() throws URISyntaxException {
        builder.authority("spiderbiggen.com");
        assertEquals("//spiderbiggen.com", builder.buildString());
    }

    @Test
    public void schemeOnly() {
        assertThrows(URISyntaxException.class, () -> builder.scheme("http").build());
    }

    @Test
    public void pathOnly() throws URISyntaxException {
        builder.appendPath("path");
        assertEquals("/path/", builder.buildString());
    }

    @Test
    public void appendQueryParameter() throws URISyntaxException {
        builder.appendQueryParameter("key", "value");
        assertEquals("?key=value", builder.buildString());
    }

    @Test
    public void appendQueryParameterMultiple() throws URISyntaxException {
        builder.appendQueryParameter("key", "value")
                .appendQueryParameter("key2", "value2")
                .appendQueryParameter("key3", "value3");
        assertEquals("?key2=value2&key3=value3&key=value", builder.buildString());
    }

    @Test
    public void appendQueryParameterObject() throws URISyntaxException {
        builder.appendQueryParameter("key", true);
        assertEquals("?key=true", builder.buildString());
    }

    @Test
    public void appendFragmentParameter() throws URISyntaxException {
        builder.appendFragmentParameter("key", "value");
        assertEquals("#key=value", builder.buildString());
    }

    @Test
    public void appendFragmentParameterObject() throws URISyntaxException {
        builder.appendFragmentParameter("key", true);
        assertEquals("#key=true", builder.buildString());
    }

    @Test
    public void appendFragmentParameterMultiple() throws URISyntaxException {
        builder.appendFragmentParameter("key", "value")
                .appendFragmentParameter("key2", "value2")
                .appendFragmentParameter("key3", "value3");
        assertEquals("#key2=value2&key3=value3&key=value", builder.buildString());
    }

    @Test
    public void fullUrl() throws URISyntaxException {
        UriBuilder builder = new UriBuilder("http", "steamcommunity.com");
        String builtUri = builder.appendPath("market")
                .appendPath("priceoverview").appendQueryParameter("appid", 5000)
                .appendQueryParameter("currency", 3)
                .appendQueryParameter("market_hash_name", "Market Hash")
                .buildString();
        String expected = "http://steamcommunity.com/market/priceoverview/?appid=5000&market_hash_name=Market%20Hash&currency=3";
        assertEquals(expected, builtUri);
    }
}