package com.spiderbiggen.steampricetracker.util;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.models.History;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.models.ItemHistory;
import org.h2.tools.Server;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoUnit.MINUTES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DatabaseTest {
    private static final LocalDateTime minDate = LocalDateTime.MIN.withYear(0);
    private static final String TEST_DATA_APP_NAME = "PLAYERUNKNOWN'S BATTLEGROUNDS";
    private static final Currency TEST_DATA_CURRENCY = Currency.EUR;
    private static final String TEST_DATA_APPID = "578080";
    private static final String TEST_DATA_MARKET_HASH0 = "DESPERADO CRATE";
    private static final String TEST_DATA_MARKET_HASH1 = "SURVIVOR CRATE";
    private static final String TEST_DATA_MARKET_HASH2 = "BIKER CRATE";
    private static Server server;
    private Currency currency;
    private String appid;
    private String randomString;

    @BeforeAll
    public static void init() throws SQLException {
        server = Server.createTcpServer().start();
        Database.setDirectory("./test_database/");
        Database.loadScript(DatabaseTest.class.getResourceAsStream("/history.h2.sql"));
    }

    @AfterAll
    public static void restore() {
        Database.resetBaseUrl();
        Database.resetDirectory();
        server.shutdown();
        File file = new File("test_database");
        deleteDir(file);
    }

    private static boolean deleteDir(File file) {
        File[] i = file.listFiles();
        if (i != null) {
            for (File file1 : i) {
                deleteDir(file1);
            }
        }
        return file.delete();
    }

    private static void verifyTables() {
        List<Table> expected = Arrays.asList(
                new Table("STEAM", "GAMES", new HashMap<String, String>() {{
                    put("APPID", "VARCHAR");
                    put("NAME", "TEXT");
                }}),
                new Table("STEAM", "HISTORY", new HashMap<String, String>() {{
                    put("ID", "INTEGER");
                    put("ITEMID", "INTEGER");
                    put("PRICE", "REAL");
                    put("DATE", "TIMESTAMP");
                    put("CURRENCY", "VARCHAR");
                }}),
                new Table("STEAM", "ITEMS", new HashMap<String, String>() {{
                    put("ID", "INTEGER");
                    put("MARKETHASH", "VARCHAR");
                    put("APPID", "VARCHAR");
                }})
        );
        List<Table> tables = new ArrayList<>();
        try {
            Database.initDatabase();
            try (Connection connection = Database.createConnection(); ResultSet resultSet = connection.getMetaData().getTables(null, "STEAM", null, null)) {
                while (resultSet.next()) {
                    tables.add(Table.parseMetaData(resultSet.getString(11)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertEquals(expected, tables);
    }

    @BeforeEach
    public void setUp() {
        Database.setDirectory("./test_database/");
        Random random = new Random();
        currency = Currency.values()[random.nextInt(Currency.values().length)];
        appid = String.valueOf(random.nextInt(10000));
        randomString = UUID.randomUUID().toString();
    }

    @AfterEach
    public void tearDown() {
        Database.resetDbName();
        Database.resetBaseUrl();
        Database.resetDirectory();
        Database.isChanged();
    }

    @Test
    public void insertPriceHistoryMulti() {
        List<History> historyList = Arrays.asList(
                new History(LocalDateTime.now().minus(1, MINUTES), 2.0, currency),
                new History(LocalDateTime.now(), 3.0, currency)
        );
        Database.insertPriceHistoryMulti(appid, randomString, historyList);
        List<History> histories = Database.getPrices(minDate, appid, randomString, currency);
        assertEquals(historyList, histories);
        assertTrue(Database.isChanged());
    }

    @Test
    public void insertPriceHistory() {
        History history = new History(LocalDateTime.now(), 2.5, currency);
        Database.insertPriceHistory(appid, randomString, history);
        Optional<History> history1 = Database.getLatestPrice(appid, randomString, currency);
        assertTrue(history1.isPresent());
        assertEquals(history, history1.get());
        assertTrue(Database.isChanged());
    }

    @Test
    public void insertGame() {
        String appName = UUID.randomUUID().toString();
        Database.insertGame(new Game(appid, appName));
        Optional<String> name = Database.getGame(appid);
        assertTrue(name.isPresent());
        assertEquals(appName, name.get());
        assertTrue(Database.isChanged());
    }

    @Test
    public void getGame() {
        Optional<String> name = Database.getGame(TEST_DATA_APPID);
        assertTrue(name.isPresent());
        assertEquals(TEST_DATA_APP_NAME, name.get());
    }

    @Test
    public void getLatestUpdate() {
        Optional<LocalDateTime> update = Database.getLatestUpdate(minDate, TEST_DATA_APPID, TEST_DATA_MARKET_HASH0, TEST_DATA_CURRENCY);
        assertTrue(update.isPresent());
        assertEquals(LocalDateTime.of(2018, 1, 22, 14, 0), update.get());
    }

    @Test
    public void getLatestPrice() {
        History history = new History(LocalDateTime.of(2018, 1, 22, 14, 0), 0.871, TEST_DATA_CURRENCY);
        Optional<History> update = Database.getLatestPrice(TEST_DATA_APPID, TEST_DATA_MARKET_HASH0, TEST_DATA_CURRENCY);
        assertTrue(update.isPresent());
        assertEquals(history, update.get());
    }

    @Test
    public void getLowestPrice() {
        History history = new History(LocalDateTime.of(2018, 1, 15, 17, 0), 0.695, TEST_DATA_CURRENCY);
        Optional<History> update = Database.getLowestPrice(minDate, TEST_DATA_APPID, TEST_DATA_MARKET_HASH0, TEST_DATA_CURRENCY);
        assertTrue(update.isPresent());
        assertEquals(history, update.get());
    }

    @Test
    public void getHighestPrice() {
        History history = new History(LocalDateTime.of(2018, 1, 12, 8, 0), 4.751, TEST_DATA_CURRENCY);
        Optional<History> update = Database.getHighestPrice(minDate, TEST_DATA_APPID, TEST_DATA_MARKET_HASH0, TEST_DATA_CURRENCY);
        assertTrue(update.isPresent());
        assertEquals(history, update.get());
    }

    @Test
    public void getPrices() {
        List<History> historyList = Arrays.asList(
                new History(LocalDateTime.of(2018, 1, 22, 1, 0, 0), 1.030, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 2, 0, 0), 0.997, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 3, 0, 0), 1.006, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 4, 0, 0), 1.015, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 5, 0, 0), 1.026, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 6, 0, 0), 1.000, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 7, 0, 0), 0.981, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 8, 0, 0), 0.981, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 9, 0, 0), 0.962, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 10, 0, 0), 0.948, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 11, 0, 0), 0.923, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 12, 0, 0), 0.918, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 13, 0, 0), 0.902, TEST_DATA_CURRENCY),
                new History(LocalDateTime.of(2018, 1, 22, 14, 0, 0), 0.871, TEST_DATA_CURRENCY)
        );
        LocalDateTime afterDate = LocalDateTime.of(2018, 1, 22, 0, 0);
        List<History> histories = Database.getPrices(afterDate, TEST_DATA_APPID, TEST_DATA_MARKET_HASH0, TEST_DATA_CURRENCY);
        assertEquals(historyList, histories);
    }

    @Test
    public void getItemValues() {
        List<Item> items = Arrays.asList(
                new Item(TEST_DATA_APPID, TEST_DATA_MARKET_HASH0),
                new Item(TEST_DATA_APPID, TEST_DATA_MARKET_HASH1),
                new Item(TEST_DATA_APPID, TEST_DATA_MARKET_HASH2)
        );
        List<ItemHistory> expected = Arrays.asList(
                new ItemHistory(TEST_DATA_APP_NAME, TEST_DATA_MARKET_HASH0, 0.695, 4.751, 0.871, TEST_DATA_CURRENCY, LocalDateTime.of(2018, 1, 22, 14, 0)),
                new ItemHistory(TEST_DATA_APP_NAME, TEST_DATA_MARKET_HASH1, 0.319, 2.959, 0.321, TEST_DATA_CURRENCY, LocalDateTime.of(2018, 1, 22, 14, 0)),
                new ItemHistory(TEST_DATA_APP_NAME, TEST_DATA_MARKET_HASH2, 0.645, 2.08, 0.645, TEST_DATA_CURRENCY, LocalDateTime.of(2018, 1, 22, 14, 0))
        );
        List<ItemHistory> histories = Database.getItemValues(items, TEST_DATA_CURRENCY, minDate);
        assertEquals(expected, histories);
    }

    @Test
    public void setDbName() {
        Database.setDbName(randomString);
        assertEquals(randomString, Database.getDbName());
    }

    @Test
    public void resetDbName() {
        Database.setDbName(randomString);
        Database.resetDbName();
        assertEquals(Database.DB_NAME, Database.getDbName());
    }

    @Test
    public void setDirectory() {
        Database.setDirectory(randomString);
        assertEquals(randomString, Database.getDirectory());
    }

    @Test
    public void resetDirectory() {
        Database.setDirectory(randomString);
        Database.resetDirectory();
        assertEquals(Database.DEFAULT_DIRECTORY, Database.getDirectory());
    }

    @Test
    public void setBaseUrl() {
        Database.setBaseUrl(randomString);
        assertEquals(randomString, Database.getBaseUrl());
    }

    @Test
    public void resetBaseUrl() {
        Database.setBaseUrl(randomString);
        Database.resetBaseUrl();
        assertEquals(Database.BASE_URL, Database.getBaseUrl());
    }

    @Test
    public void ExportImport() {
        try {
            Database.dump(Database.getDirectory());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Database.setDbName("test_init");
        File file = new File("test_database/history.h2.sql");
        try {
            InputStream stream = new FileInputStream(file);
            Database.loadScript(stream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        verifyTables();
    }

    @Test
    public void initDatabase() {
        Database.setDbName("test_init");
        verifyTables();
    }

    private static class Table {
        private String schema;
        private String tableName;
        private Map<String, String> columns;

        private Table(String schema, String tableName, Map<String, String> columns) {
            this.schema = schema;
            this.tableName = tableName;
            this.columns = columns;
        }

        private Table() {
            columns = new HashMap<>();
        }

        private static Table parseMetaData(String in) {
            Table table = new Table();
            String[] lines = in.replaceAll("\\([0-9]+\\)|[(),]", "").split("[\n]");
            Pattern pat = Pattern.compile("(\\w+\\.\\w+)$");
            Matcher matcher = pat.matcher(lines[0]);
            if (matcher.find()) {
                String[] st = matcher.group().split("\\.");
                table.schema = st[0];
                table.tableName = st[1];
            }
            for (int i = 1; i < lines.length; i++) {
                String[] line = lines[i].replaceAll("^[\\s]+", "").split(" ");
                table.columns.put(line[0], line[1]);
            }
            return table;
        }

        @Override
        public int hashCode() {
            return Objects.hash(schema, tableName, columns);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Table table = (Table) o;
            return Objects.equals(schema, table.schema) &&
                    Objects.equals(tableName, table.tableName) &&
                    Objects.equals(columns, table.columns);
        }

        @Override
        public String toString() {
            return "Table{" +
                    "schema='" + schema + '\'' +
                    ", tableName='" + tableName + '\'' +
                    ", columns=" + columns +
                    '}';
        }
    }
}