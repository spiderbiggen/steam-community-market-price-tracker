package com.spiderbiggen.steampricetracker.util;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import static com.spiderbiggen.steampricetracker.util.HttpHelper.HttpRequestMode.GET;
import static com.spiderbiggen.steampricetracker.util.HttpHelper.HttpRequestMode.POST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HttpHelperTest {

    @Test
    public void httpRequest() throws IOException {
        URL url = new URL("http://httpbin.org/get");
        Optional<String> json = HttpHelper.httpRequest(url);
        assertTrue(json.isPresent());
    }

    @Test
    public void httpRequestError() throws IOException {
        URL url = new URL("http://httpbin.org/status/404");
        assertThrows(IOException.class, () -> HttpHelper.httpRequest(url));
    }

    @Test
    public void httpRequest1() throws IOException {
        URL url = new URL("http://httpbin.org/post");
        Optional<String> json = HttpHelper.httpRequest(url, POST);
        assertTrue(json.isPresent());
    }

    @Test
    public void httpRequest1Error() throws IOException {
        URL url = new URL("http://httpbin.org/post");
        assertThrows(IOException.class, () -> HttpHelper.httpRequest(url, GET));
    }

    @Test
    public void httpRequestWithCookie() throws IOException {
        URL url = new URL("http://httpbin.org/cookies");
        Optional<String> json = HttpHelper.httpRequestWithCookie(url, "key=val");
        assertTrue(json.isPresent());
        JSONObject object = new JSONObject(json.get());
        JSONObject cookies = object.getJSONObject("cookies");
        assertEquals("val", cookies.getString("key"));
    }

    @Test
    public void httpRequestWithMultipleCookies() throws IOException {
        URL url = new URL("http://httpbin.org/cookies");
        Optional<String> json = HttpHelper.httpRequestWithCookie(url, "key=val; key2=val2; key3=val3");
        assertTrue(json.isPresent());
        JSONObject object = new JSONObject(json.get());
        JSONObject cookies = object.getJSONObject("cookies");
        assertEquals("val", cookies.getString("key"));
        assertEquals("val2", cookies.getString("key2"));
        assertEquals("val3", cookies.getString("key3"));
    }

}