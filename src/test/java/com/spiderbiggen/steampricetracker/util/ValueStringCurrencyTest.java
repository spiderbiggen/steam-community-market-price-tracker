package com.spiderbiggen.steampricetracker.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ValueStringCurrencyTest {

    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Currency.USD, 1.0, "$1.00"},
                {Currency.GBP, 1.0, "\u00a31.00"},
                {Currency.EUR, 1.0, "1.00\u20ac"},
                {Currency.CHF, 1.0, "CHF 1.00"},
                {Currency.RUB, 1.0, "1.00 p\u0443\u0431."},
                {Currency.BRL, 1.0, "R$ 1.00"},
                {Currency.JPY, 1.0, "\u00a5 1.00"},
                {Currency.SEK, 1.0, "1.00 kr"},
                {Currency.IDR, 1.0, "Rp 17 1.00"},
                {Currency.MYR, 1.0, "MR1.00"},
                {Currency.PHP, 1.0, "P1.00"},
                {Currency.SGD, 1.0, "S$1.00"},
                {Currency.THB, 1.0, "\u0e3f1.00"},
                {Currency.KRW, 1.0, "\u20a9 1.00"},
                {Currency.TRY, 1.0, "1.00 TL"},
                {Currency.MXN, 1.0, "Mex$ 1.00"},
                {Currency.CAD, 1.0, "CDN$ 1.00"},
                {Currency.NZD, 1.0, "NZ$ 1.00"},
                {Currency.CNY, 1.0, "\u00a5 1.00"},
                {Currency.INR, 1.0, "\u20b9 1.00"},
                {Currency.CLP, 1.0, "CLP$ 1.00"},
                {Currency.PEN, 1.0, "S/ 1.00"},
                {Currency.COP, 1.0, "COL$ 1.00"},
                {Currency.ZAR, 1.0, "R 1.00"},
                {Currency.HKD, 1.0, "HK$ 1.00"},
                {Currency.TWD, 1.0, "NT$ 1.00"},
                {Currency.SAR, 1.0, "1.00 SR"},
                {Currency.AED, 1.0, "1.00 AED"}
        });
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testValueSring(Currency currency, double value, String result) {
        assertEquals(result, currency.getValueString(value));
    }
}