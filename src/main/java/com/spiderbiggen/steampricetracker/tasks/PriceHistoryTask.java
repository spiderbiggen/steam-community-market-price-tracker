package com.spiderbiggen.steampricetracker.tasks;

import com.spiderbiggen.steampricetracker.models.History;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.util.Currency;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.HttpHelper;
import com.spiderbiggen.steampricetracker.util.Settings;
import com.spiderbiggen.steampricetracker.util.UriBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

public class PriceHistoryTask implements Runnable {

    private static Locale locale = Locale.getDefault();

    public PriceHistoryTask() {

    }

    @Override
    public void run() {
        String token = Settings.getToken();
        Set<Item> items = Settings.getItems();

        for (Item item : items) {
            String appid = item.getAppid();
            String marketHash = item.getMarkethash();

            if (Settings.hasCurrency()) {
                Optional<LocalDateTime> date = Database.getLatestUpdate(LocalDateTime.now().minusHours(2), appid, marketHash, Settings.getCurrency());
                if (date.isPresent()) continue;
            }
            getHistoryTask(appid, marketHash, token);
        }
    }

    private void getHistoryTask(String appid, String marketHash, String token) {
        Optional<String> response = marketHistoryRequest(appid, marketHash, token);
        response.ifPresent(res -> {
            JSONObject object = new JSONObject(res);
            boolean success = object.getBoolean("success");
            if (!success) return;
            Currency currency;
            if (Settings.hasCurrency()) {
                currency = Settings.getCurrency();
            } else {
                currency = Currency.fromPrefixAndSuffix(object.getString("price_prefix"), object.getString("price_suffix"));
                Settings.setCurrency(currency);
            }
            JSONArray array = object.getJSONArray("prices");
            DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd uuuu HH", Locale.ENGLISH);
            List<History> data = new ArrayList<>();
            IntStream.range(0, array.length()).mapToObj(array::getJSONArray).forEach(hist -> {
                LocalDateTime dateTime = LocalDateTime.parse(hist.getString(0).split(":")[0], format);
                double price = hist.getDouble(1);
                data.add(new History(dateTime, price, currency));
            });
            Database.insertPriceHistoryMulti(appid, marketHash, data);
        });
    }

    private Optional<String> marketHistoryRequest(String appid, String marketHash, String token) {
        try {
            UriBuilder builder = new UriBuilder();
            String builtUri = builder.scheme("http").authority("steamcommunity.com").appendPath("market")
                    .appendPath("pricehistory").appendQueryParameter("country", locale.getISO3Country())
                    .appendQueryParameter("appid", appid)
                    .appendQueryParameter("market_hash_name", marketHash)
                    .buildString();

            String cookieString = "steamLogin=" + token; // + ";"
            URL url = new URL(builtUri);
            return HttpHelper.httpRequestWithCookie(url, cookieString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
