package com.spiderbiggen.steampricetracker.tasks;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.tasks.callbacks.AppNameCallback;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.HttpHelper;
import com.spiderbiggen.steampricetracker.util.UriBuilder;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;

public class GetAppNameTask implements Runnable {

    private String appid;
    private AppNameCallback callback;
    private boolean overwrite = false;

    public GetAppNameTask(String appid, AppNameCallback callback) {
        this.appid = appid;
        this.callback = callback;
    }

    public GetAppNameTask overwrite() {
        overwrite = true;
        return this;
    }

    @Override
    public void run() {
        if (appid == null) return;
        Optional<String> appName;
        if (!overwrite) {
            appName = Database.getGame(appid);
            if (appName.isPresent()) {
                callback(appName.get());
                return;
            }
        }
        appName = getAppName(this.appid);
        appName.ifPresent(app -> Database.insertGame(new Game(appid, app)));
        appName.ifPresent(this::callback);
    }

    private Optional<String> getAppName(String appid) {
        Optional<String> response = appDetailRequest(appid);
        return response.map(res -> {
            JSONObject object = new JSONObject(res);
            JSONObject app = object.getJSONObject(appid);
            boolean success = app.getBoolean("success");
            if (!success) return null;
            JSONObject data = app.getJSONObject("data");
            return data.getString("name");
        });
    }

    private Optional<String> appDetailRequest(String appid) {
        try {
            UriBuilder builder = new UriBuilder();
            String builtUri = builder.scheme("http").authority("store.steampowered.com")
                    .appendPath("api").appendPath("appdetails")
                    .appendQueryParameter("appids", appid)
                    .appendQueryParameter("filters", "basic")
                    .buildString();
            URL url = new URL(builtUri);
            return HttpHelper.httpRequest(url);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private void callback(String appName) {
        if (callback != null) {
            callback.setAppName(appName);
        }
    }
}
