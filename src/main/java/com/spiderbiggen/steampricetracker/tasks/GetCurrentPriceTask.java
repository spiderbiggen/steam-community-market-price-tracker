package com.spiderbiggen.steampricetracker.tasks;

import com.spiderbiggen.steampricetracker.models.History;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.util.Currency;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.HttpHelper;
import com.spiderbiggen.steampricetracker.util.Settings;
import com.spiderbiggen.steampricetracker.util.UriBuilder;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class GetCurrentPriceTask implements Runnable {

    @Override
    public void run() {
        Currency currency = Settings.getCurrency();
        Set<Item> items = Settings.getItems();
        items.forEach(item -> updatePrice(item.getAppid(), item.getMarkethash(), currency));
    }

    private void updatePrice(String appid, String marketHash, Currency currency) {
        Optional<String> response = requestMarketUpdate(appid, marketHash, currency);
        response.ifPresent(res -> {
            JSONObject object = new JSONObject(res);
            boolean success = object.getBoolean("success");
            if (!success) return;
            String priceString = object.getString("lowest_price");
            priceString = priceString.substring(currency.getPrefix().length(), priceString.length() - currency.getSuffix().length());
            priceString = priceString.replace(",", "").replace(".", "");
            priceString = priceString.substring(0, priceString.length() - 2) + "." + priceString.substring(priceString.length() - 2);
            double price = Double.parseDouble(priceString);
            Database.insertPriceHistory(appid, marketHash, new History(LocalDateTime.now(), price, currency));
        });
    }

    private Optional<String> requestMarketUpdate(String appid, String marketHash, Currency currency) {
        try {
            UriBuilder builder = new UriBuilder("http", "steamcommunity.com");
            String builtUri = builder.appendPath("market")
                    .appendPath("priceoverview").appendQueryParameter("appid", appid)
                    .appendQueryParameter("currency", currency.getIndex())
                    .appendQueryParameter("market_hash_name", marketHash)
                    .buildString();
            return HttpHelper.httpRequest(new URL(builtUri));
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


}
