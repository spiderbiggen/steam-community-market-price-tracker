package com.spiderbiggen.steampricetracker.tasks.callbacks;

/**
 * Defines an callback object that can receive an app name.
 */
public interface AppNameCallback {

    /**
     * Set the app name of this object.
     *
     * @param appName The name of the game.
     */
    void setAppName(String appName);
}
