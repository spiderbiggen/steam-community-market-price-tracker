package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.fx.ToggleablePasswordField;
import com.spiderbiggen.steampricetracker.ui.TabController;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Currency;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.ResourceBundle;

public class UserSettingsView extends TabController {

    @FXML
    private ToggleablePasswordField token;
    @FXML
    private ComboBox<Currency> currencyComboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void open() {
        ObservableList<Currency> currencies;
        Currency currency;
        token.setText(Settings.getToken());
        currencyComboBox.setDisable(true);
        //TODO Fix this nonsense
        if (Settings.hasCurrency()) {
            currency = Settings.getCurrency();
            if (currency == Currency.CNY || currency == Currency.JPY) {
                currencies = FXCollections.observableArrayList(Currency.JPY, Currency.CNY);
                currencyComboBox.setDisable(false);
            } else {
                currencies = FXCollections.singletonObservableList(currency);
            }
            currencyComboBox.setItems(currencies);
            currencyComboBox.setValue(currency);
        }

    }

    @Override
    public void close() {
        token.hide();
        Settings.setToken(token.getText());
        if (Settings.hasCurrency()) {
            Settings.setCurrency(currencyComboBox.getValue());
        }
    }

    @Override
    public View getView() {
        return View.USER_SETTINGS;
    }

}
