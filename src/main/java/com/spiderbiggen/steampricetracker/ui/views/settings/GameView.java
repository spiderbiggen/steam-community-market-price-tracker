package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.tasks.GetAppNameTask;
import com.spiderbiggen.steampricetracker.tasks.callbacks.AppNameCallback;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.ui.views.ViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class GameView extends ViewController implements AppNameCallback {

    @FXML
    private TitledPane container;
    @FXML
    private Label appidField;
    @FXML
    private TextField gameTitleField;

    private Game game = new Game(null, null);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        appidField.textProperty().bind(game.appidProperty());
        gameTitleField.textProperty().bindBidirectional(game.nameProperty());
        container.textProperty().bind(game.nameProperty());
    }

    @FXML
    private void resetGameTitle(ActionEvent actionEvent) {
        if (game.getAppid() != null)
            new Thread(new GetAppNameTask(game.getAppid(), this).overwrite()).start();
    }

    /**
     * Gets game
     *
     * @return value of game
     */
    public Game getGame() {
        return game;
    }

    /**
     * Sets the game.
     *
     * @param game the new game.
     */
    public void setGame(Game game) {
        if (game == null) return;
        this.game.setAppid(game.getAppid());
        this.game.setName(game.getName());
    }

    public TitledPane getContainer() {
        return container;
    }

    @Override
    public void setAppName(String appName) {
        game.setName(appName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGame());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameView gameView = (GameView) o;
        return Objects.equals(getGame(), gameView.getGame());
    }

    @Override
    public View getView() {
        return View.GAME;
    }
}
