package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import com.spiderbiggen.steampricetracker.ui.TabController;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

public class ItemSettingsView extends TabController {

    @FXML
    private Accordion itemHolder;
    private Set<ItemView> itemViewList = new HashSet<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void open() {
        itemViewList.clear();
        itemHolder.getPanes().clear();
        Set<Item> items = Settings.getItems();
        items.forEach(this::addItemView);
    }

    @Override
    public void close() {
        Settings.setItems(itemViewList.stream().map(ItemView::getItem)
                .filter(i -> i.getAppid() != null).filter(i -> i.getMarkethash() != null)
                .collect(Collectors.toList()));
    }

    public void removeGameView(ItemView itemView, TitledPane container) {
        itemViewList.remove(itemView);
        itemHolder.getPanes().remove(container);
    }

    @FXML
    private void createItemView(ActionEvent actionEvent) {
        addItemView(null);
    }

    private void addItemView(Item item) {
        GuiFactory.createItemView(item).ifPresent(this::registerItemView);
    }

    private void registerItemView(ItemView itemView) {
        if (itemViewList.add(itemView)) {
            itemHolder.getPanes().add(itemView.getContainer());
            itemView.setItemSettingsView(this);
        }
    }

    @Override
    public View getView() {
        return View.ITEM_SETTINGS;
    }
}
