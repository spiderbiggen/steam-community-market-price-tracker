package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import com.spiderbiggen.steampricetracker.ui.TabController;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Database;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class GameSettingsView extends TabController {

    @FXML
    private Accordion gameHolder;
    private Set<GameView> gameViews = new HashSet<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void open() {
        gameViews.clear();
        gameHolder.getPanes().clear();
        List<Game> items = Database.getGames();
        items.forEach(this::addGameView);
    }

    @Override
    public void close() {
        gameViews.stream().map(GameView::getGame).forEach(Database::insertGame);
    }

    @Override
    public View getView() {
        return View.GAME_SETTINGS;
    }

    private void addGameView(Game game) {
        GuiFactory.createGameView(game).ifPresent(this::registerGameView);
    }

    private void registerGameView(GameView gameView) {
        if (gameViews.add(gameView)) {
            gameHolder.getPanes().add(gameView.getContainer());
        }
    }
}
