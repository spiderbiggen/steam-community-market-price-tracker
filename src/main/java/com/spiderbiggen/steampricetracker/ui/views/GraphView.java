package com.spiderbiggen.steampricetracker.ui.views;

import com.spiderbiggen.steampricetracker.fx.DateAxis;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Currency;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

public class GraphView extends ViewController {

    @FXML
    private LineChart<LocalDateTime, Double> log;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private DateAxis xAxis;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.setCreateSymbols(false);
        log.setCursor(Cursor.CROSSHAIR);
        log.setAnimated(false);
        xAxis.setAnimated(false);
    }

    /**
     * Update UI Asynchronously. All ui updating methods must call {@link Platform#runLater(Runnable)}
     */
    @Override
    public void updateUI() {
        long num = Settings.getIntervalLength();
        ChronoUnit unit = Settings.getIntervalUnit();
        com.spiderbiggen.steampricetracker.util.Currency currency = Settings.getCurrency();
        Set<Item> items = Settings.getItems();
        LocalDateTime dateTime = LocalDateTime.now().minus(num, unit);

        List<Series<LocalDateTime, Double>> series = items.parallelStream()
                .map(item -> new Series<>(item.getMarkethash(), FXCollections.observableList(
                        Database.getPrices(dateTime, item.getAppid(), item.getMarkethash(), currency).parallelStream()
                                .map(h -> new XYChart.Data<>(h.getDate(), h.getPrice()))
                                .collect(Collectors.toList())
                ))).collect(Collectors.toList());
        updateGraph(currency, series);
    }

    /**
     * Update the graph view with the given data, can be called Asynchronously.
     *
     * @param currency       currency that was used to retrieve the priceHistory
     * @param priceHistories Price history
     */
    private void updateGraph(Currency currency, List<Series<LocalDateTime, Double>> priceHistories) {
        Platform.runLater(() -> {
            log.getData().clear();
            yAxis.setLabel(currency.toString());

            for (Series<LocalDateTime, Double> priceHistory : priceHistories) {
                if (priceHistory == null) continue;
                log.getData().add(priceHistory);
            }
        });
    }

    @Override
    public View getView() {
        return View.GRAPH;
    }
}
