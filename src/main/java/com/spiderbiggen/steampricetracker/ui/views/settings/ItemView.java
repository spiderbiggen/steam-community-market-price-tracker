package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.tasks.GetAppNameTask;
import com.spiderbiggen.steampricetracker.tasks.callbacks.AppNameCallback;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.ui.views.ViewController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class ItemView extends ViewController implements AppNameCallback {

    @FXML
    private TitledPane container;
    @FXML
    private TextField appidField;
    @FXML
    private TextField marketHashField;

    private Item item = new Item(null, null);

    private ItemSettingsView itemSettingsView;
    private String appname;

    /**
     * Sets itemSettingsView.
     *
     * @param itemSettingsView the new value of itemSettingsView
     */
    public void setItemSettingsView(ItemSettingsView itemSettingsView) {
        this.itemSettingsView = itemSettingsView;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        appidField.focusedProperty().addListener(observable -> requestAppName());
        appidField.textProperty().bindBidirectional(item.appidProperty());
        marketHashField.focusedProperty().addListener(observable -> updateTitle(appname, item.getMarkethash()));
        marketHashField.textProperty().bindBidirectional(item.markethashProperty());
        updateTitle(null, item.getMarkethash());
        requestAppName();
    }

    private void setTitle(String title) {
        container.setText(title);
    }

    private void updateTitle(String appName, String marketHash) {
        String text = "Undefined";
        if (appName != null || marketHash != null) {
            String game = appName == null ? "Undefined" : appName;
            String item = marketHash == null ? "Undefined" : marketHash;
            text = String.format("%s - %s", game, item);
        }
        setTitle(text);
    }

    private void requestAppName() {
        if (item.getAppid() != null)
            new Thread(new GetAppNameTask(item.getAppid(), this)).start();
    }

    @FXML
    private void removeItem(ActionEvent actionEvent) {
        itemSettingsView.removeGameView(this, container);
    }

    /**
     * Gets item
     *
     * @return value of item
     */
    public Item getItem() {
        return item;
    }

    /**
     * Sets item.
     *
     * @param appid      the new value of item.appid
     * @param marketHash the new value of item.marketHash
     */
    public void setItem(String appid, String marketHash) {
        item.setAppid(appid);
        requestAppName();
        item.setMarkethash(marketHash);
    }

    public TitledPane getContainer() {
        return container;
    }

    @Override
    public void setAppName(String appName) {
        this.appname = appName;
        Platform.runLater(() -> updateTitle(appName, item.getMarkethash()));
    }

    @Override
    public int hashCode() {

        return Objects.hash(getItem());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemView itemView = (ItemView) o;
        return Objects.equals(getItem(), itemView.getItem());
    }

    @Override
    public View getView() {
        return View.ITEM;
    }
}
