package com.spiderbiggen.steampricetracker.ui.views;

import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.models.ItemHistory;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Currency;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class CurrentView extends ViewController {

    @FXML
    private TableView<ItemHistory> tableView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (TableColumn<ItemHistory, ?> tableColumn : tableView.getColumns()) {
            String propertyName = tableColumn.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tableColumn.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }
    }

    /**
     * Update UI Asynchronously. All ui updating methods must call {@link Platform#runLater(Runnable)}
     */
    @Override
    public void updateUI() {
        long num = Settings.getIntervalLength();
        ChronoUnit unit = Settings.getIntervalUnit();
        Currency currency = Settings.getCurrency();
        Set<Item> items = Settings.getItems();
        LocalDateTime dateTime = LocalDateTime.now().minus(num, unit);

        Database.getItemValues(items, currency, dateTime);
        updateTable(Database.getItemValues(items, currency, dateTime));
    }

    /**
     * Update the graph view with the given data, can be called Asynchronously.
     *
     * @param priceHistories Price history
     */
    private void updateTable(List<ItemHistory> priceHistories) {
        Platform.runLater(() -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(priceHistories);
        });
    }

    @Override
    public View getView() {
        return View.CURRENT;
    }
}
