package com.spiderbiggen.steampricetracker.ui.views;

import com.spiderbiggen.steampricetracker.Main;
import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import com.spiderbiggen.steampricetracker.ui.TabController;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class SettingsView extends ViewController {

    private static final View[] views = {View.ITEM_SETTINGS, View.GAME_SETTINGS, View.USER_SETTINGS, View.APPLICATION_SETTINGS};

    @FXML
    private TabPane tabPane;
    private Map<Tab, TabController> tabControllerMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Arrays.stream(views).map(GuiFactory::createTab).forEach(c -> c.ifPresent(this::registerController));
        open();
        tabPane.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    controllerFromTab(oldValue).ifPresent(TabController::close);
                    controllerFromTab(newValue).ifPresent(TabController::open);
                });
    }

    private Optional<TabController> controllerFromTab(Tab tab) {
        TabController controller = null;
        if (tabControllerMap.containsKey(tab))
            controller = tabControllerMap.get(tab);
        return Optional.ofNullable(controller);
    }

    private void registerController(TabController tabController) {
        tabControllerMap.put(tabController.getTab(), tabController);
        tabPane.getTabs().add(tabController.getTab());
    }

    @Override
    public void open() {
        tabControllerMap.values().forEach(ViewController::open);
    }

    @Override
    public void close() {
        tabControllerMap.values().forEach(ViewController::close);
        Main.updatePricehistory();
        try {
            Settings.saveProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View getView() {
        return View.SETTINGS;
    }

}
