package com.spiderbiggen.steampricetracker.ui.views.settings;

import com.spiderbiggen.steampricetracker.fx.NumberField;
import com.spiderbiggen.steampricetracker.ui.StackPaneTransition;
import com.spiderbiggen.steampricetracker.ui.TabController;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.time.temporal.ChronoUnit;
import java.util.ResourceBundle;

public class ApplicationSettingsView extends TabController {

    private static final ChronoUnit[] TIME_UNITS = new ChronoUnit[]{
            ChronoUnit.DAYS, ChronoUnit.WEEKS, ChronoUnit.MONTHS, ChronoUnit.YEARS, ChronoUnit.DECADES
    };

    @FXML
    private NumberField numberOfInterval;
    @FXML
    private ComboBox<ChronoUnit> intervalSelector;
    @FXML
    private ComboBox<StackPaneTransition.TransitionType> transitionComboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        transitionComboBox.setItems(FXCollections.observableArrayList(StackPaneTransition.TransitionType.values()));
        intervalSelector.setItems(FXCollections.observableArrayList(TIME_UNITS));
    }

    @Override
    public void open() {
        numberOfInterval.setText(String.valueOf(Settings.getIntervalLength()));

        transitionComboBox.setValue(Settings.getTransitionType());
        intervalSelector.setValue(Settings.getIntervalUnit());
    }

    @Override
    public void close() {
        Settings.setTransitionType(transitionComboBox.getValue());
        Settings.setIntervalUnit(intervalSelector.getValue());
        Settings.setIntervalLength(Long.parseLong(numberOfInterval.getText()));
    }

    @Override
    public View getView() {
        return View.APPLICATION_SETTINGS;
    }
}
