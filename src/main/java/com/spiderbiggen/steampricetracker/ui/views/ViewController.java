package com.spiderbiggen.steampricetracker.ui.views;

import com.spiderbiggen.steampricetracker.ui.StackPaneTransition;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public abstract class ViewController implements Initializable {

    @FXML
    private Node root;

    public void destroy() {
        // Do nothing
    }

    public void updateUI() {
        // Do nothing
    }

    public void open() {
        // Do nothing
    }

    public void close() {
        // Do nothing
    }

    public void transition() {
        StackPaneTransition.transition((StackPane) root.getParent(), root, Settings.getTransitionType());
    }

    public Node getRoot() {
        return root;
    }

    /**
     * Gets The View Enum
     *
     * @return value of this view
     */
    public abstract View getView();
}
