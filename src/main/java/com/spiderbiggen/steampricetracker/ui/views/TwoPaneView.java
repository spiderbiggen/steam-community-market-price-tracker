package com.spiderbiggen.steampricetracker.ui.views;

import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.net.URL;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TwoPaneView extends ViewController {

    private static final View[] views = {View.GRAPH, View.CURRENT, View.SETTINGS};
    private static View currentScreenId = null;

    private final Map<View, ViewController> controllerMap = new Hashtable<>();
    private ScheduledExecutorService executorService;

    @FXML
    private VBox sidebarMenu;
    @FXML
    private Button hamburger;
    @FXML
    private StackPane container;
    @FXML
    private ToggleGroup toggleGroup;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Arrays.stream(views).map(GuiFactory::createSubPage).forEach(c -> c.ifPresent(this::registerController));
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::updateUI, 0, 1, TimeUnit.SECONDS);
        toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == null) toggleGroup.selectToggle(oldValue);
        });
        changeScreen(View.GRAPH);
    }


    @FXML
    private void openView(ActionEvent event) {
        Object source = event.getSource();
        if (source instanceof Node) {
            Node node = (Node) source;
            Object userData = node.getUserData();
            if (userData instanceof String) {
                View view = View.valueOf((String) userData);
                changeScreen(view);
            }
        }
    }

    private void changeScreen(View view) {
        if (view == currentScreenId) return;
        if (controllerMap.containsKey(view)) {
            if (currentScreenId != null) {
                controllerMap.get(currentScreenId).close();
            }
            currentScreenId = view;
            ViewController controller = controllerMap.get(view);
            controller.open();
            controller.transition();
        }
    }

    @FXML
    private void toggleSideBar(ActionEvent event) {
        Node hamburger = this.hamburger.getGraphic();
        boolean menuClosed = hamburger.getRotate() == 0;
        double newWidth = menuClosed ? 150 : 40;
        double newRotation = menuClosed ? 90 : 0;
        final Timeline timeline = new Timeline();
        final KeyFrame kf = new KeyFrame(Duration.millis(100), new KeyValue(hamburger.rotateProperty(), newRotation));
        final KeyFrame kf2 = new KeyFrame(Duration.millis(180), new KeyValue(sidebarMenu.prefWidthProperty(), newWidth));
        timeline.getKeyFrames().addAll(kf, kf2);
        timeline.play();
    }

    @Override
    public void destroy() {
        controllerMap.values().forEach(ViewController::destroy);
        executorService.shutdown();
    }

    @Override
    public void updateUI() {
        if (Database.isChanged() || Settings.isChanged()) {
            controllerMap.values().forEach(ViewController::updateUI);
        }
    }

    @Override
    public void transition() {
        // Do nothing
    }

    @Override
    public View getView() {
        return View.TWO_PANE;
    }

    private void registerController(ViewController c) {
        controllerMap.put(c.getView(), c);
        container.getChildren().add(c.getRoot());
    }
}
