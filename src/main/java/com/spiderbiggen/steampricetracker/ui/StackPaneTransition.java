package com.spiderbiggen.steampricetracker.ui;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class StackPaneTransition {
    private final ObservableList<Node> nodes;
    private Node node;

    private StackPaneTransition(Node node, StackPane container) {
        nodes = container.getChildren();
        this.node = node;
    }

    public static StackPaneTransition transition(StackPane container, Node node) {
        return new StackPaneTransition(node, container);
    }

    public static void transition(StackPane container, Node node, TransitionType type) {
        StackPaneTransition transition = new StackPaneTransition(node, container);
        switch (type) {
            case FADE:
                transition.fade();
                break;
            case TRANSLATE:
                transition.translate();
                break;
        }
    }

    private void prepareTransition() {
        nodes.remove(node);
        nodes.add(nodes.size() - 1, node);
        node = nodes.get(nodes.size() - 1);
    }

    private void fade() {
        prepareTransition();
        FadeTransition ft = new FadeTransition(Duration.millis(300), node);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(e -> {
            nodes.remove(node);
            nodes.add(0, node);
            node.setOpacity(1.0);
        });
        ft.play();
    }

    private void translate() {
        prepareTransition();
        TranslateTransition ft = new TranslateTransition(Duration.millis(300), node);
        ft.setFromX(0);
        ft.setToX(((Pane) node).getWidth());
        ft.setOnFinished(e -> {
            nodes.remove(node);
            nodes.add(0, node);
            node.setTranslateX(0);
        });
        ft.play();
    }

    public enum TransitionType {
        FADE, TRANSLATE;
    }
}
