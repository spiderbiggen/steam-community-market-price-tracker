package com.spiderbiggen.steampricetracker.ui;

import com.spiderbiggen.steampricetracker.util.LocalizedString;

public enum View {
    TWO_PANE("/fxml/two_pane_view.fxml", "Home"),
    SETTINGS("/fxml/property_view.fxml", "Settings"),
    GRAPH("/fxml/graph_view.fxml", "Histogram"),
    CURRENT("/fxml/current_view.fxml", "Current"),
    ITEM_SETTINGS("/fxml/settings/item_settings_view.fxml", "Items"),
    GAME_SETTINGS("/fxml/settings/game_settings_view.fxml", "Games"),
    USER_SETTINGS("/fxml/settings/user_settings_view.fxml", "User"),
    APPLICATION_SETTINGS("/fxml/settings/application_settings_view.fxml", "Application"),
    ITEM("/fxml/settings/item_view.fxml", "Item"),
    GAME("/fxml/settings/game_view.fxml", "Game");

    private final String layout;
    private final LocalizedString title;

    View(String layout, String title) {
        this.layout = layout;
        this.title = new LocalizedString("views." + this.name(), title);
    }

    /**
     * Gets layout
     *
     * @return value of layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * Gets title
     *
     * @return value of title
     */
    public LocalizedString getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title.toString();
    }
}
