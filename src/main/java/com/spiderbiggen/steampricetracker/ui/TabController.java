package com.spiderbiggen.steampricetracker.ui;

import com.spiderbiggen.steampricetracker.ui.views.ViewController;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;

public abstract class TabController extends ViewController {

    @FXML
    private Tab tab;

    /**
     * Gets tab
     *
     * @return value of tab
     */
    public Tab getTab() {
        return tab;
    }
}
