package com.spiderbiggen.steampricetracker.ui;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.ui.views.ViewController;
import com.spiderbiggen.steampricetracker.ui.views.settings.GameView;
import com.spiderbiggen.steampricetracker.ui.views.settings.ItemView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

public class GuiFactory {

    private static final String MAIN_TITLE = "Steam Community Market Price Tracker";

    public static ViewController createStage(Stage primaryStage, View view) throws IOException {
        Image windowIcon = new Image(GuiFactory.class.getResource("/images/icon.png").toExternalForm());
        FXMLLoader loader = new FXMLLoader(GuiFactory.class.getResource(view.getLayout()));
        Parent root = loader.load();
        Scene scene = new Scene(root, 700, 400);
        scene.getStylesheets().add(GuiFactory.class.getResource("/css/style.css").toExternalForm());
        primaryStage.getIcons().add(windowIcon);
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.setScene(scene);
        primaryStage.setTitle(MAIN_TITLE);
        primaryStage.show();
        return loader.getController();
    }

    public static Optional<ViewController> createSubPage(View view) {
        ViewController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(GuiFactory.class.getResource(view.getLayout()));
            loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(controller);
    }

    private static Alert createErrorAlert(String title, String header, String exceptionText) {
        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(header);
        alert.setTitle(title);
        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
        return alert;
    }

    public static Alert createErrorAlert(String alertTitle, Exception e) {
        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        return createErrorAlert(alertTitle, e.getMessage(), exceptionText);
    }

    public static Optional<TabController> createTab(View view) {
        TabController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(GuiFactory.class.getResource(view.getLayout()));
            loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return Optional.ofNullable(controller);
    }

    public static Optional<ItemView> createItemView(Item item) {
        if (item == null) return createItemView(null, null);
        return createItemView(item.getAppid(), item.getMarkethash());
    }


    private static Optional<ItemView> createItemView(String appid, String marketHash) {
        ItemView controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(GuiFactory.class.getResource("/fxml/settings/item_view.fxml"));
            loader.load();
            controller = loader.getController();
            controller.setItem(appid, marketHash);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return Optional.ofNullable(controller);
    }

    public static Optional<GameView> createGameView(Game game) {
        GameView controller = null;
        if (game != null) {
            try {
                FXMLLoader loader = new FXMLLoader(GuiFactory.class.getResource("/fxml/settings/game_view.fxml"));
                loader.load();
                controller = loader.getController();
                controller.setGame(game);
            } catch (IOException e) {
                createErrorAlert(GuiFactory.class.getSimpleName(), e);
            }
        }
        return Optional.ofNullable(controller);
    }
}
