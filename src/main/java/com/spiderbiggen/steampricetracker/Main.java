package com.spiderbiggen.steampricetracker;

import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.tasks.GetAppNameTask;
import com.spiderbiggen.steampricetracker.tasks.GetCurrentPriceTask;
import com.spiderbiggen.steampricetracker.tasks.PriceHistoryTask;
import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import com.spiderbiggen.steampricetracker.ui.View;
import com.spiderbiggen.steampricetracker.ui.views.ViewController;
import com.spiderbiggen.steampricetracker.util.Database;
import com.spiderbiggen.steampricetracker.util.Settings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.h2.tools.Server;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main extends Application {

    private static Server server;
    private static ScheduledExecutorService executorService;
    private ViewController controller;

    public static void main(String... args) {
        try {
            Settings.setupProperties();
            // start the TCP Server
            server = Server.createTcpServer().start();
            Database.initDatabase();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        launch(args);
    }

    public static void updatePricehistory() {
        new Thread(new PriceHistoryTask()).start();
    }

    private static void connectBot() {
        updatePricehistory();
        Set<Item> items = Settings.getItems();
        items.forEach(i -> Database.getGame(i.getAppid()).orElseGet(() -> {
            new Thread(new GetAppNameTask(i.getAppid(), null)).start();
            return null;
        }));
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new GetCurrentPriceTask(), 1, 5, TimeUnit.MINUTES);
    }

    private static void disconnectBot() {
        if (executorService != null)
            executorService.shutdown();
    }

    @Override
    public void start(Stage primaryStage) {
        connectBot();
        try {
            controller = GuiFactory.createStage(primaryStage, View.TWO_PANE);
        } catch (Exception e) {
            e.printStackTrace();
            Platform.exit();
        }
    }

    @Override
    public void stop() {
        controller.destroy();
        disconnectBot();
        // stop the TCP Server
        server.stop();

    }
}
