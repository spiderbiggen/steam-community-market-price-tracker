package com.spiderbiggen.steampricetracker.fx;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;


public class ToggleablePasswordField extends HBox {
    private TextInputControl passwordField;
    private StringProperty promptText = new SimpleStringProperty();
    private ToggleButton toggleButton;
    
    public ToggleablePasswordField() {
        super();
        getStyleClass().add("password-toggle");
        this.toggleButton = new ToggleButton();
        this.toggleButton.setGraphic(new Region());
        ObservableList<String> buttonStyleClass = this.toggleButton.getStyleClass();
        buttonStyleClass.add("password-toggle");
        ObservableList<String> graphicStyleClass = this.toggleButton.getGraphic().getStyleClass();
        graphicStyleClass.addAll("eye-closed", "icon-graphic");
        setPasswordField(new PasswordField());
        getChildren().add(this.toggleButton);
        toggleButton.setOnAction(this::onToggle);
    }
    
    public final StringProperty promptTextProperty() {
        return promptText;
    }
    
    public final String getPromptText() {
        return promptText.get();
    }
    
    public final void setPromptText(String value) {
        promptText.set(value);
    }
    
    private void onToggle(ActionEvent event) {
        TextInputControl field;
        ObservableList<String> graphicStyleClass = this.toggleButton.getGraphic().getStyleClass();
        if (toggleButton.isSelected()) {
            field = new TextField(passwordField.getText());
            graphicStyleClass.removeAll("eye-closed");
            graphicStyleClass.add("eye-open");
        } else {
            field = new PasswordField();
            field.setText(passwordField.getText());
            graphicStyleClass.removeAll("eye-open");
            graphicStyleClass.add("eye-closed");
        }
        field.promptTextProperty().bind(promptText);
        setPasswordField(field);
    }
    
    public String getText() {
        return passwordField.getText();
    }
    
    public void setText(String text) {
        passwordField.setText(text);
    }
    
    private void setPasswordField(TextInputControl control) {
        getChildren().remove(passwordField);
        passwordField = control;
        HBox.setHgrow(passwordField, Priority.ALWAYS);
        getChildren().add(0, passwordField);
    }

    public void hide() {
        toggleButton.setSelected(false);
        onToggle(null);
    }
}
