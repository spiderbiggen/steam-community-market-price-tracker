package com.spiderbiggen.steampricetracker.fx;

import javafx.scene.control.TextField;

/**
 * Created on 19-6-2017.
 *
 * @author Stefan Breetveld
 */
public class NumberField extends TextField {

    @Override
    public void replaceText(int start, int end, String text)
    {
        if (validate(text))
        {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text)
    {
        if (validate(text))
        {
            super.replaceSelection(text);
        }
    }

    private boolean validate(String text)
    {
        return text.matches("[0-9]*");
    }
}
