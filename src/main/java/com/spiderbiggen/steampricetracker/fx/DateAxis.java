/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013, Christian Schudt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.spiderbiggen.steampricetracker.fx;

import com.sun.javafx.charts.ChartLayoutAnimator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleLongProperty;
import javafx.scene.chart.Axis;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.time.temporal.ChronoUnit.*;

/**
 * An axis that displays date and time values.
 * Tick labels are usually automatically set and calculated depending on the range unless you explicitly {@linkplain #setTickLabelFormatter(StringConverter)}  set a formatter}.
 * You also have the chance to specify fix lower and upper bounds, otherwise they are calculated by your data.
 *
 * Modified to take {@link OffsetDateTime} instead of {@link Date} objects.
 * @author Stefan Breetveld
 * @see <a href="https://bitbucket.org/sco0ter/extfx">https://bitbucket.org/sco0ter/extfx</a>
 */
public final class DateAxis extends Axis<LocalDateTime> {

    /**
     * These property are used for animation.
     */
    private final LongProperty currentLowerBound = new SimpleLongProperty(this, "currentLowerBound");

    private final LongProperty currentUpperBound = new SimpleLongProperty(this, "currentUpperBound");

    private final ObjectProperty<StringConverter<LocalDateTime>> tickLabelFormatter = new ObjectPropertyBase<StringConverter<LocalDateTime>>() {
        @Override
        protected void invalidated() {
            if (!isAutoRanging()) {
                invalidateRange();
                requestAxisLayout();
            }
        }

        @Override
        public Object getBean() {
            return DateAxis.this;
        }

        @Override
        public String getName() {
            return "tickLabelFormatter";
        }
    };

    /**
     * Stores the min and max date of the list of dates which is used.
     * If {@link #autoRanging} is true, these values are used as lower and upper bounds.
     */
    private LocalDateTime minDate, maxDate;

    private ObjectProperty<LocalDateTime> lowerBound = new ObjectPropertyBase<LocalDateTime>() {
        @Override
        protected void invalidated() {
            if (!isAutoRanging()) {
                invalidateRange();
                requestAxisLayout();
            }
        }

        @Override
        public Object getBean() {
            return DateAxis.this;
        }

        @Override
        public String getName() {
            return "lowerBound";
        }
    };

    private ObjectProperty<LocalDateTime> upperBound = new ObjectPropertyBase<LocalDateTime>() {
        @Override
        protected void invalidated() {
            if (!isAutoRanging()) {
                invalidateRange();
                requestAxisLayout();
            }
        }

        @Override
        public Object getBean() {
            return DateAxis.this;
        }

        @Override
        public String getName() {
            return "upperBound";
        }
    };

    private ChartLayoutAnimator animator = new ChartLayoutAnimator(this);

    private Object currentAnimationID;

    private Interval actualInterval = Interval.DECADE;

    /**
     * Default constructor. By default the lower and upper bound are calculated by the data.
     */
    public DateAxis() {
    }

    /**
     * Constructs a date axis with fix lower and upper bounds.
     *
     * @param lowerBound The lower bound.
     * @param upperBound The upper bound.
     */
    public DateAxis(LocalDateTime lowerBound, LocalDateTime upperBound) {
        this();
        setAutoRanging(false);
        setLowerBound(lowerBound);
        setUpperBound(upperBound);
    }

    /**
     * Constructs a date axis with a label and fix lower and upper bounds.
     *
     * @param axisLabel  The label for the axis.
     * @param lowerBound The lower bound.
     * @param upperBound The upper bound.
     */
    public DateAxis(String axisLabel, LocalDateTime lowerBound, LocalDateTime upperBound) {
        this(lowerBound, upperBound);
        setLabel(axisLabel);
    }

    @Override
    public void invalidateRange(List<LocalDateTime> list) {
        super.invalidateRange(list);

        Collections.sort(list);
        if (list.isEmpty()) {
            minDate = maxDate = LocalDateTime.now();
        } else if (list.size() == 1) {
            minDate = maxDate = list.get(0);
        } else {
            minDate = list.get(0);
            maxDate = list.get(list.size() - 1);
        }
    }

    @Override
    protected Object autoRange(double length) {
        if (isAutoRanging()) {
            return new Object[]{minDate, maxDate};
        } else {
            if (getLowerBound() == null || getUpperBound() == null) {
                throw new IllegalArgumentException("If autoRanging is false, a lower and upper bound must be set.");
            }
            return getRange();
        }
    }

    @Override
    protected void setRange(Object range, boolean animating) {
        Object[] r = (Object[]) range;
        LocalDateTime oldLowerBound = getLowerBound();
        LocalDateTime oldUpperBound = getUpperBound();
        LocalDateTime lower = (LocalDateTime) r[0];
        LocalDateTime upper = (LocalDateTime) r[1];
        setLowerBound(lower);
        setUpperBound(upper);

        if (animating) {
            animator.stop(currentAnimationID);
            currentAnimationID = animator.animate(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(currentLowerBound, oldLowerBound.toEpochSecond(ZoneOffset.UTC)),
                            new KeyValue(currentUpperBound, oldUpperBound.toEpochSecond(ZoneOffset.UTC))
                    ),
                    new KeyFrame(Duration.millis(700),
                            new KeyValue(currentLowerBound, lower.toEpochSecond(ZoneOffset.UTC)),
                            new KeyValue(currentUpperBound, upper.toEpochSecond(ZoneOffset.UTC))
                    )
            );

        } else {
            currentLowerBound.set(getLowerBound().toEpochSecond(ZoneOffset.UTC));
            currentUpperBound.set(getUpperBound().toEpochSecond(ZoneOffset.UTC));
        }
    }

    @Override
    protected Object getRange() {
        return new Object[]{getLowerBound(), getUpperBound()};
    }

    @Override
    public double getZeroPosition() {
        return 0;
    }

    @Override
    public double getDisplayPosition(LocalDateTime date) {
        final double length = getSide().isHorizontal() ? getWidth() : getHeight();

        // Get the difference between the max and min date.
        double diff = currentUpperBound.get() - currentLowerBound.get();

        // Get the actual range of the visible area.
        // The minimal date should start at the zero position, that's why we subtract it.
        double range = length - getZeroPosition();

        // Then get the difference from the actual date to the min date and divide it by the total difference.
        // We get a value between 0 and 1, if the date is within the min and max date.
        double d = (date.toEpochSecond(ZoneOffset.UTC) - currentLowerBound.get()) / diff;

        // Multiply this percent value with the range and the zero offset.
        if (getSide().isVertical()) {
            return getHeight() - d * range + getZeroPosition();
        } else {
            return d * range + getZeroPosition();
        }
    }

    @Override
    public LocalDateTime getValueForDisplay(double displayPosition) {
        final double length = getSide().isHorizontal() ? getWidth() : getHeight();

        // Get the difference between the max and min date.
        double diff = currentUpperBound.get() - currentLowerBound.get();

        // Get the actual range of the visible area.
        // The minimal date should start at the zero position, that's why we subtract it.
        double range = length - getZeroPosition();

        if (getSide().isVertical()) {
            // displayPosition = getHeight() - ((date - lowerBound) / diff) * range + getZero
            // date = displayPosition - getZero - getHeight())/range * diff + lowerBound
            return LocalDateTime.ofEpochSecond((long) ((displayPosition - getZeroPosition() - getHeight()) / -range * diff + currentLowerBound.get()), 0 , ZoneOffset.UTC);
        } else {
            // displayPosition = ((date - lowerBound) / diff) * range + getZero
            // date = displayPosition - getZero)/range * diff + lowerBound
            return LocalDateTime.ofEpochSecond((long) ((displayPosition - getZeroPosition()) / range * diff + currentLowerBound.get()), 0 , ZoneOffset.UTC);
        }
    }

    @Override
    public boolean isValueOnAxis(LocalDateTime date) {
        return date.toEpochSecond(ZoneOffset.UTC) > currentLowerBound.get() && date.toEpochSecond(ZoneOffset.UTC) < currentUpperBound.get();
    }

    @Override
    public double toNumericValue(LocalDateTime date) {
        return date.toEpochSecond(ZoneOffset.UTC);
    }

    @Override
    public LocalDateTime toRealValue(double v) {
        return LocalDateTime.ofEpochSecond((long) v, 0, ZoneOffset.UTC);
    }

    @Override
    protected List<LocalDateTime> calculateTickValues(double v, Object range) {
        Object[] r = (Object[]) range;
        LocalDateTime lower = (LocalDateTime) r[0];
        LocalDateTime upper = (LocalDateTime) r[1];

        List<LocalDateTime> dateList = new ArrayList<>();
        LocalDateTime counter;

        // The preferred gap which should be between two tick marks.
        double averageTickGap = 100;
        double averageTicks = v / averageTickGap;

        List<LocalDateTime> previousDateList = new ArrayList<>();

        Interval previousInterval = Interval.values()[0];

        // Starting with the greatest interval, of one of each calendar unit.
        for (Interval interval : Interval.values()) {
            // Reset the calendar.
            counter = lower;
            // Clear the list.
            dateList.clear();
            previousDateList.clear();
            actualInterval = interval;

            // Loop as long we exceeded the upper bound.
            while (!counter.isAfter(upper)) {
                dateList.add(counter);
                counter = counter.plus(interval.amount, interval.interval);
            }
            // Then check the size of the list. If it is greater than the amount of ticks, take that list.
            if (dateList.size() > averageTicks) {
                counter = lower;
                // Recheck if the previous interval is better suited.
                while (!counter.isAfter(upper)) {
                    previousDateList.add(counter);
                    counter = counter.plus(previousInterval.amount, previousInterval.interval);
                }
                break;
            }

            previousInterval = interval;
        }
        if (previousDateList.size() - averageTicks > averageTicks - dateList.size()) {
            dateList = previousDateList;
            actualInterval = previousInterval;
        }
        return dateList;
    }

    @Override
    protected void layoutChildren() {
        if (!isAutoRanging()) {
            currentLowerBound.set(getLowerBound().toEpochSecond(ZoneOffset.UTC));
            currentUpperBound.set(getUpperBound().toEpochSecond(ZoneOffset.UTC));
        }
        super.layoutChildren();
    }

    @Override
    protected String getTickMarkLabel(LocalDateTime date) {

        StringConverter<LocalDateTime> converter = getTickLabelFormatter();
        if (converter != null) {
            return converter.toString(date);
        }

        DateTimeFormatter dateFormat;

        if (actualInterval.interval.equals(YEARS) && date.getMonth().equals(Month.JANUARY) && date.getDayOfMonth() == 1) {
            dateFormat = DateTimeFormatter.ofPattern("yyyy");
        } else if (actualInterval.interval.equals(MONTHS) && date.getDayOfMonth() == 1) {
            dateFormat = DateTimeFormatter.ofPattern("MMM yy");
        } else {
            switch (actualInterval.interval) {
                case DAYS:
                case WEEKS:
                default:
                    dateFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
                    break;
                case HOURS:
                case MINUTES:
                    dateFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
                    break;
            }
        }
        return dateFormat.format(date);
    }

    /**
     * Gets the lower bound of the axis.
     *
     * @return The property.
     * @see #getLowerBound()
     * @see #setLowerBound(LocalDateTime)
     */
    public final ObjectProperty<LocalDateTime> lowerBoundProperty() {
        return lowerBound;
    }

    /**
     * Gets the lower bound of the axis.
     *
     * @return The lower bound.
     * @see #lowerBoundProperty()
     */
    public final LocalDateTime getLowerBound() {
        return lowerBound.get();
    }

    /**
     * Sets the lower bound of the axis.
     *
     * @param date The lower bound date.
     * @see #lowerBoundProperty()
     */
    public final void setLowerBound(LocalDateTime date) {
        lowerBound.set(date);
    }

    /**
     * Gets the upper bound of the axis.
     *
     * @return The property.
     * @see #getUpperBound() ()
     * @see #setUpperBound(LocalDateTime)
     */
    public final ObjectProperty<LocalDateTime> upperBoundProperty() {
        return upperBound;
    }

    /**
     * Gets the upper bound of the axis.
     *
     * @return The upper bound.
     * @see #upperBoundProperty()
     */
    public final LocalDateTime getUpperBound() {
        return upperBound.get();
    }

    /**
     * Sets the upper bound of the axis.
     *
     * @param date The upper bound date.
     * @see #upperBoundProperty() ()
     */
    public final void setUpperBound(LocalDateTime date) {
        upperBound.set(date);
    }

    /**
     * Gets the tick label formatter for the ticks.
     *
     * @return The converter.
     */
    public final StringConverter<LocalDateTime> getTickLabelFormatter() {
        return tickLabelFormatter.getValue();
    }

    /**
     * Sets the tick label formatter for the ticks.
     *
     * @param value The converter.
     */
    public final void setTickLabelFormatter(StringConverter<LocalDateTime> value) {
        tickLabelFormatter.setValue(value);
    }

    /**
     * Gets the tick label formatter for the ticks.
     *
     * @return The property.
     */
    public final ObjectProperty<StringConverter<LocalDateTime>> tickLabelFormatterProperty() {
        return tickLabelFormatter;
    }

    /**
     * The intervals, which are used for the tick labels. Beginning with the largest interval, the axis tries to calculate the tick values for this interval.
     * If a smaller interval is better suited for, that one is taken.
     */
    private enum Interval {
        DECADE(DECADES, 1),
        YEAR(YEARS, 1),
        MONTH_6(MONTHS, 6),
        MONTH_3(MONTHS, 3),
        MONTH_1(MONTHS, 1),
        WEEK(WEEKS, 1),
        DAY(DAYS, 1),
        HOUR_12(HOURS, 12),
        HOUR_6(HOURS, 6),
        HOUR_3(HOURS, 3),
        HOUR_1(HOURS, 1),
        MINUTE_15(MINUTES, 15),
        MINUTE_5(MINUTES, 5),
        MINUTE_1(MINUTES, 1),
        SECOND_15(SECONDS, 15),
        SECOND_5(SECONDS, 5),
        SECOND_1(SECONDS, 1),
        MILLISECOND(MILLIS, 1);

        private final int amount;

        private final ChronoUnit interval;

        Interval(ChronoUnit interval, int amount) {
            this.interval = interval;
            this.amount = amount;
        }

    }

    private OffsetDateTime getTimeFromEpoch(long time) {
        return Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toOffsetDateTime();
    }
}