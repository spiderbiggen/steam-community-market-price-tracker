package com.spiderbiggen.steampricetracker.util;

import com.spiderbiggen.steampricetracker.models.Game;
import com.spiderbiggen.steampricetracker.models.History;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.models.ItemHistory;
import com.spiderbiggen.steampricetracker.ui.GuiFactory;
import javafx.application.Platform;
import org.h2.tools.Recover;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class Database {

    public static final String DEFAULT_DIRECTORY = "./database/";
    public static final String DB_NAME = "history";
    public static final String BASE_URL = "tcp://localhost/";
    private static final String ALERT_TITLE = "Database Error";
    private static final String JDBC_TYPE = "jdbc:h2:";
    private static AtomicBoolean changed = new AtomicBoolean(true);
    private static AtomicReference<String> dbName = new AtomicReference<>(DB_NAME);
    private static AtomicReference<String> dir = new AtomicReference<>(DEFAULT_DIRECTORY);
    private static AtomicReference<String> baseUrl = new AtomicReference<>(BASE_URL);

    private Database() {
    }

    public static boolean isChanged() {
        return changed.getAndSet(false);
    }

    private static void setChanged() {
        Database.changed.lazySet(true);
    }

    public static void initDatabase() throws SQLException {
        createSchema();
        createItemReferenceTable();
        createHistoryTable();
        createGameReferenceTable();
    }

    private static void createSchema() throws SQLException {
        //language=H2
        String sql = "CREATE SCHEMA IF NOT EXISTS STEAM";
        // create a connection to the database
        try (Connection connection = createSimpleConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    private static Connection createSimpleConnection() throws SQLException {
        String url = JDBC_TYPE + baseUrl.get() + dir.get() + dbName.get();
        // create a connection to the database
        return createConnection(url);
    }

    /**
     * Connect to a sample database
     */
    public static Connection createConnection() throws SQLException {
        String url = JDBC_TYPE + baseUrl.get() + dir.get() + dbName.get() + ";SCHEMA=STEAM";
        // create a connection to the database
        return createConnection(url);
    }

    private static Connection createConnection(String url) throws SQLException {
        return DriverManager.getConnection(url, "main", "main");
    }

    private static void createItemReferenceTable() throws SQLException {
        //language=H2
        final String sql = "CREATE TABLE IF NOT EXISTS items ("
                + "	id INTEGER AUTO_INCREMENT PRIMARY KEY,"
                + "	marketHash VARCHAR(50) NOT NULL,"
                + " appid VARCHAR(10) NOT NULL,"
                + "UNIQUE(marketHash, appid)"
                + ");";
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    private static void createGameReferenceTable() throws SQLException {
        //language=H2
        final String sql = "CREATE TABLE IF NOT EXISTS games ("
                + " appid VARCHAR(10) PRIMARY KEY,"
                + " name TEXT NOT NULL,"
                + "UNIQUE(appid)"
                + ");";
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    private static void createHistoryTable() throws SQLException {
        //language=H2
        final String sql = "CREATE TABLE IF NOT EXISTS history ("
                + " id INTEGER AUTO_INCREMENT PRIMARY KEY,"
                + "	itemId INTEGER NOT NULL,"
                + "	price REAL,"
                + "	date TIMESTAMP,"
                + "	currency VARCHAR(3),"
                + " UNIQUE(itemId, date, currency),"
                + " FOREIGN KEY (itemId) REFERENCES items(id)"
                + ");";

        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    public static void insertPriceHistoryMulti(String appid, String marketHash, List<History> elements) {
        //language=H2
        final String insertSql = "MERGE INTO history(itemId, price, date, currency) KEY(itemId, date, currency) "
                + "VALUES ( ?, ?, ?, ?);";
        Optional<Long> id = getItemId(appid, marketHash);
        if (!id.isPresent()) {
            id = insertItem(appid, marketHash);
        }
        id.ifPresent(rowID -> {
            try (Connection connection = createConnection()) {
                boolean oldAutoCommit = connection.getAutoCommit();
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(insertSql)) {
                    int count = 0;
                    for (History value : elements) {
                        ps.setLong(1, rowID);
                        ps.setDouble(2, value.getPrice());
                        ps.setTimestamp(3, Timestamp.valueOf(value.getDate()));
                        ps.setString(4, value.getCurrency().name());
                        ps.addBatch();
                        if (++count % 1000 == 0) {
                            ps.executeBatch();
                        }
                    }
                    ps.executeBatch();
                }

                connection.commit();
                setChanged();
                connection.setAutoCommit(oldAutoCommit);
            } catch (SQLException e) {
                showErrorMessage(e);
            }
        });
    }

    public static void insertPriceHistory(String appid, String marketHash, History history) {
        //language=H2
        final String insertSql = "MERGE INTO history(itemId, price, date, currency) KEY(itemId, date, currency) " +
                " VALUES (?, ?, ?, ?);";
        Optional<Long> id = getItemId(appid, marketHash);
        if (!id.isPresent()) {
            id = insertItem(appid, marketHash);
        }
        id.ifPresent(row -> {
            try (Connection connection = createConnection();
                 PreparedStatement ps = connection.prepareStatement(insertSql)) {
                ps.setLong(1, row);
                ps.setDouble(2, history.getPrice());
                ps.setTimestamp(3, Timestamp.valueOf(history.getDate()));
                ps.setString(4, history.getCurrency().name());
                ps.executeUpdate();
                setChanged();
            } catch (SQLException e) {
                showErrorMessage(e);
            }
        });

    }

    private static Optional<Long> getItemId(String appid, String marketHash) {
        //language=H2
        final String sql = "SELECT id FROM items" +
                " WHERE marketHash IS ? AND appid = ?;";

        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, marketHash);
            ps.setString(2, appid);
            try (ResultSet rs = ps.executeQuery()) {
                // loop through the result set
                Long row = null;
                if (rs.next()) row = rs.getLong("id");
                return Optional.ofNullable(row);
            }
        } catch (SQLException e) {
            showErrorMessage(e);
        }
        return Optional.empty();
    }

    /**
     * Inserts an item into the item table if it doesn't exist, otherwise it does nothing.
     *
     * @param appid      appid corresponding to the game that has this item
     * @param marketHash the full unescaped name addPair the item
     * @return the id primary key associated with the created item
     */
    private static Optional<Long> insertItem(String appid, String marketHash) {
        //language=H2
        final String insertSql = "MERGE INTO items(appid, marketHash) KEY(appid, marketHash)" +
                " VALUES (?, ?);";
        return insertIntoTable(insertSql, appid, marketHash);
    }

    /**
     * Inserts a game into the game table if it doesn't exist, otherwise it updates the entry.
     *
     * @param game the game that should be added.
     */
    public static Optional<Long> insertGame(Game game) {
        if (game.getAppid() == null || game.getName() == null) return Optional.empty();
        //language=H2
        final String insertSql = "MERGE INTO games(appid, name) " +
                " VALUES (?, ?);";
        return insertIntoTable(insertSql, game.getAppid(), game.getName());
    }

    public static List<Game> getGames() {
        List<Game> games = new ArrayList<>();
        //language=H2
        final String selectSql = "SELECT * FROM GAMES;";
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(selectSql)) {
            while (resultSet.next()) {
                games.add(new Game(resultSet.getString("appid"), resultSet.getString("name")));
            }
        } catch (SQLException e) {
            showErrorMessage(e);
        }
        return games;
    }

    private static Optional<Long> insertIntoTable(String sql, String... par1) {
        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < par1.length; ++i) {
                ps.setString(i + 1, par1[i]);
            }
            ps.executeUpdate();
            setChanged();
            try (ResultSet keys = ps.getGeneratedKeys()) {
                if (keys.next()) {
                    return Optional.of(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            showErrorMessage(e);
        }
        return Optional.empty();
    }

    public static Optional<String> getGame(String appid) {
        //language=H2
        String sql = "SELECT name FROM games WHERE appid = ?;";

        try (Connection connection = createConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, appid);
            try (ResultSet rs = ps.executeQuery()) {
                // loop through the result set
                if (rs.next()) return Optional.ofNullable(rs.getString("name"));
            }
        } catch (SQLException e) {
            showErrorMessage(e);
        }
        return Optional.empty();
    }

    public static Optional<LocalDateTime> getLatestUpdate(LocalDateTime dateTime, String appid, String marketHash, Currency currency) {
        //language=H2
        final String sql = "SELECT date FROM history"
                + " WHERE currency IS ? AND itemId IS ?"
                + " AND date > ?"
                + " ORDER BY date DESC"
                + " LIMIT 1;";
        return getItemId(appid, marketHash).map((Long rowID) -> {
            try (Connection connection = createConnection();
                 PreparedStatement ps = priceStatement(connection, sql, rowID, dateTime, currency);
                 ResultSet rs = ps.executeQuery()) {
                // loop through the result set
                if (rs.next()) return rs.getTimestamp("date").toLocalDateTime();
            } catch (SQLException e) {
                showErrorMessage(e);
            }
            return null;
        });

    }

    public static Optional<History> getLatestPrice(String appid, String marketHash, Currency currency) {
        //language=H2
        final String sql = "SELECT ROUND(price, 3) AS price, date FROM history"
                + " WHERE currency IS ? AND itemId IS ?"
                + " AND date > ?"
                + " ORDER BY date DESC LIMIT 1;";

        return getBoundaryPrice(sql, LocalDateTime.MIN.withYear(0), appid, marketHash, currency);
    }

    public static Optional<History> getLowestPrice(LocalDateTime dateTime, String appid, String marketHash, Currency currency) {
        //language=H2
        final String sql = "SELECT ROUND(price, 3) AS price, date FROM history"
                + " WHERE currency IS ? AND itemId IS ?"
                + " AND date > ?"
                + " ORDER BY price ASC LIMIT 1;";
        return getBoundaryPrice(sql, dateTime, appid, marketHash, currency);
    }

    public static Optional<History> getHighestPrice(LocalDateTime dateTime, String appid, String marketHash, Currency currency) {
        //language=H2
        final String sql = "SELECT ROUND(price, 3) AS price, date FROM history"
                + " WHERE currency IS ? AND itemId IS ?"
                + " AND date > ?"
                + " ORDER BY price DESC LIMIT 1;";
        return getBoundaryPrice(sql, dateTime, appid, marketHash, currency);
    }

    private static Optional<History> getBoundaryPrice(String sql, LocalDateTime dateTime, String appid, String marketHash, Currency currency) {
        Optional<Long> id = getItemId(appid, marketHash);
        return id.map(rowID -> {
            try (Connection connection = createConnection();
                 PreparedStatement ps = priceStatement(connection, sql, rowID, dateTime, currency);
                 ResultSet rs = ps.executeQuery()) {
                // loop through the result set
                if (rs.next()) {
                    return new History(rs.getTimestamp("date").toLocalDateTime(), rs.getDouble("price"), currency);
                }
            } catch (SQLException e) {
                showErrorMessage(e);
            }
            return null;
        });
    }

    public static List<History> getPrices(LocalDateTime date, String appid, String marketHash, Currency currency) {
        Optional<Long> id = getItemId(appid, marketHash);
        //language=H2
        final String sql = "SELECT ROUND(price, 3) AS price, date FROM history"
                + " WHERE currency IS ? AND itemId IS ?"
                + " AND date > ?";
        List<History> series = new ArrayList<>();
        id.ifPresent(rowID -> {
            try (Connection connection = createConnection();
                 PreparedStatement ps = priceStatement(connection, sql, rowID, date, currency);
                 ResultSet rs = ps.executeQuery()) {
                // loop through the result set
                while (rs.next()) {
                    double price = rs.getDouble("price");
                    LocalDateTime histDate = rs.getTimestamp("date").toLocalDateTime();
                    series.add(new History(histDate, price, currency));
                }
            } catch (SQLException e) {
                showErrorMessage(e);
            }
        });
        return series;
    }

    private static PreparedStatement priceStatement(Connection connection, String sql, long rowID, LocalDateTime dateTime, Currency currency) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, currency.name());
        ps.setLong(2, rowID);
        ps.setTimestamp(3, Timestamp.valueOf(dateTime));
        return ps;
    }

    public static List<ItemHistory> getItemValues(Collection<Item> items, Currency currency, LocalDateTime dateTime) {
        List<ItemHistory> histories = new ArrayList<>();
        items.forEach(item -> {
            double lowestPrice = getLowestPrice(dateTime, item.getAppid(), item.getMarkethash(), currency).map(History::getPrice).orElse(-1.0);
            double highestPrice = getHighestPrice(dateTime, item.getAppid(), item.getMarkethash(), currency).map(History::getPrice).orElse(-1.0);
            Optional<History> current = getLatestPrice(item.getAppid(), item.getMarkethash(), currency);
            double currentPrice = current.map(History::getPrice).orElse(-1.0);
            LocalDateTime lastUpdate = current.map(History::getDate).orElse(null);
            String game = getGame(item.getAppid()).orElse("Undefined");
            histories.add(new ItemHistory(game, item.getMarkethash(), lowestPrice, highestPrice, currentPrice, currency, lastUpdate));
        });

        return histories;
    }

    private static void showErrorMessage(SQLException e) {
        Platform.runLater(() -> GuiFactory.createErrorAlert(ALERT_TITLE, e).show());
    }

    public static String getDbName() {
        return dbName.get();
    }

    public static void setDbName(String dbName) {
        Database.dbName.set(dbName);
    }

    public static void resetDbName() {
        setDbName(DB_NAME);
    }

    public static void resetDirectory() {
        setDirectory(DEFAULT_DIRECTORY);
    }

    public static String getDirectory() {
        return dir.get();
    }

    public static void setDirectory(String dir) {
        Database.dir.set(dir);
    }

    public static void resetBaseUrl() {
        setBaseUrl(BASE_URL);
    }

    public static String getBaseUrl() {
        return baseUrl.get();
    }

    public static void setBaseUrl(String baseUrl) {
        Database.baseUrl.set(baseUrl);
    }

    public static void dump(String path) throws SQLException {
        File file = new File(path);
        boolean cont = file.isDirectory();
        if (!cont) {
            cont = file.mkdirs();
        }
        if (cont) {
            Recover.execute(path, null);
            System.out.println("Succesfully dumped database to: " + file.getAbsolutePath());
        }
    }

    /**
     * Read a properly formatted sql script.
     *
     * @param stream the stream that contains the script.
     * @return a list of queries.
     */
    private static List<String> parseScript(InputStream stream) {
        List<String> querys = new ArrayList<>();
        try (BufferedReader dbScript = new BufferedReader(new InputStreamReader(stream))) {
            String line = dbScript.readLine();
            StringBuilder query = new StringBuilder();
            while (line != null) {
                if (!line.trim().startsWith("#") && !line.trim().startsWith("--") && !line.isEmpty()) {
                    query.append(line).append(System.lineSeparator());
                    if (line.trim().endsWith(";")) {
                        querys.add(query.toString());
                        query = new StringBuilder();
                    }
                }
                line = dbScript.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return querys;
    }

    public static void loadScript(InputStream stream) {
        List<String> querys = parseScript(stream);
        try (Connection connection = createSimpleConnection();
             Statement statement = connection.createStatement()) {
            for (String query : querys) {
                statement.addBatch(query);
            }
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
