package com.spiderbiggen.steampricetracker.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

public class HttpHelper {

    private HttpHelper() {
    }

    public static Optional<String> httpRequest(URL url) throws IOException {
        return httpRequest(url, HttpRequestMode.GET);
    }

    public static Optional<String> httpRequest(URL url, HttpRequestMode requestMode) throws IOException {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(requestMode.name());
            return connect(urlConnection);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public static Optional<String> httpRequestWithCookie(URL url, String cookieString) throws IOException {
        return httpRequestWithCookie(url, cookieString, HttpRequestMode.GET);
    }

    public static Optional<String> httpRequestWithCookie(URL url, String cookieString, HttpRequestMode requestMode) throws IOException {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(requestMode.name());
            urlConnection.setRequestProperty("Cookie", cookieString);
            return connect(urlConnection);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    private static Optional<String> connect(HttpURLConnection urlConnection) throws IOException {
        BufferedReader reader = null;
        try {
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) return Optional.empty();

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append(" ");
            }

            return buffer.length() == 0 ? Optional.empty() : Optional.of(buffer.toString());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public enum HttpRequestMode {
        GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, CONNECT, PATCH
    }
}
