package com.spiderbiggen.steampricetracker.util;

import com.spiderbiggen.steampricetracker.Main;
import com.spiderbiggen.steampricetracker.models.Item;
import com.spiderbiggen.steampricetracker.ui.StackPaneTransition.TransitionType;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Settings {

    private static final String TOKEN_KEY = "TOKEN";
    private static final String CURRENCY_KEY = "CURRENCY";
    private static final String TRANSITION_TYPE_KEY = "TRANSITION_TYPE";
    private static final String INTERVAL_DURATION_KEY = "INTERVAL_DURATION";
    private static final String INTERVAL_TYPE_KEY = "INTERVAL_TYPE";
    private static final String APP_ID_PREFIX = "APP_ID";
    private static final String MARKET_HASH_PREFIX = "MARKET_HASH";
    private static final String DEFAULT_SAVE = "custom.properties";
    private static Properties applicationProps;
    private static AtomicBoolean changed = new AtomicBoolean(true);
    private static AtomicReference<String> saveFile = new AtomicReference<>(DEFAULT_SAVE);

    private static AtomicInteger count = new AtomicInteger(0);

    private Settings() {
    }

    public static boolean isChanged() {
        return changed.getAndSet(false);
    }

    private static void setChanged() {
        changed.lazySet(true);
    }

    public static void setupProperties() throws IOException {
        loadDefaults();
        applicationProps = new Properties(applicationProps);
        File file = new File(getSaveFile());
        if (file.exists()) {
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                applicationProps.load(fileInputStream);
            }
        }
    }

    public static void loadDefaults() throws IOException {
        applicationProps = new Properties();
        InputStream in = Main.class.getResourceAsStream("/config.properties");
        applicationProps.load(in);
        in.close();
    }

    public static void saveProperties() throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(getSaveFile()), "utf-8"))) {
            applicationProps.store(writer, "Config");
        }
    }

    public static Set<Item> getItems() {
        Set<String> propertyNames = applicationProps.stringPropertyNames();
        return propertyNames.stream().filter(s -> s.startsWith(APP_ID_PREFIX))
                .map(s -> Integer.parseInt(s.replace(APP_ID_PREFIX, "")))
                .map(Settings::getItem).collect(Collectors.toSet());
    }

    public static void setItems(Collection<Item> items) {
        count.set(0);
        Set<String> propertyNames = new HashSet<>(applicationProps.stringPropertyNames());
        propertyNames.removeIf(s -> !s.startsWith(APP_ID_PREFIX) && !s.startsWith(MARKET_HASH_PREFIX));
        propertyNames.forEach(Settings::removeProperty);
        items.forEach(Settings::addItem);
    }

    private static Item getItem(int id) {
        return new Item(getProperty(APP_ID_PREFIX + id), getProperty(MARKET_HASH_PREFIX + id));
    }

    private static void addItem(Item item) {
        int id = count.getAndIncrement();
        setProperty(APP_ID_PREFIX + id, item.getAppid());
        setProperty(MARKET_HASH_PREFIX + id, item.getMarkethash());
    }

    public static Set<String> getAppids() {
        return getAppidKeys().stream().map(Settings::getProperty).collect(Collectors.toSet());
    }

    public static Set<String> getAppidKeys() {
        return applicationProps.stringPropertyNames().stream().filter(s -> s.startsWith(APP_ID_PREFIX)).collect(Collectors.toSet());
    }

    public static String getToken() {
        return applicationProps.getProperty(TOKEN_KEY);
    }

    public static void setToken(String token) {
        setProperty(TOKEN_KEY, token);
    }

    public static boolean hasCurrency() {
        return hasProperty(CURRENCY_KEY);
    }

    public static Currency getCurrency() {
        return Currency.valueOf(applicationProps.getProperty(CURRENCY_KEY));
    }

    public static void setCurrency(Currency currency) {
        setProperty(CURRENCY_KEY, currency.name());
    }

    public static TransitionType getTransitionType() {
        return TransitionType.valueOf(applicationProps.getProperty(TRANSITION_TYPE_KEY));
    }

    public static void setTransitionType(TransitionType transitionType) {
        setProperty(TRANSITION_TYPE_KEY, transitionType.name());
    }

    public static long getIntervalLength() {
        return Long.parseLong(applicationProps.getProperty(INTERVAL_DURATION_KEY));
    }

    public static void setIntervalLength(long length) {
        setProperty(INTERVAL_DURATION_KEY, length);
    }

    public static ChronoUnit getIntervalUnit() {
        return ChronoUnit.valueOf(applicationProps.getProperty(INTERVAL_TYPE_KEY));
    }

    public static void setIntervalUnit(ChronoUnit unit) {
        setProperty(INTERVAL_TYPE_KEY, unit.name());
    }

    private static boolean hasProperty(String key) {
        return applicationProps.containsKey(key);
    }

    private static String getProperty(String key) {
        return applicationProps.getProperty(key);
    }

    private static void removeProperty(String key) {
        applicationProps.remove(key);
        setChanged();
    }

    private static void setProperty(String key, String value) {
        applicationProps.setProperty(key, value);
        setChanged();
    }

    private static void setProperty(String key, Object value) {
        applicationProps.setProperty(key, String.valueOf(value));
    }

    public static void clearProperties() {
        applicationProps = null;
    }

    public static void resetSaveFile() {
        setSaveFile(DEFAULT_SAVE);
    }

    public static String getSaveFile() {
        return saveFile.get();
    }

    public static void setSaveFile(String saveFile) {
        Settings.saveFile.set(saveFile);
    }

}
