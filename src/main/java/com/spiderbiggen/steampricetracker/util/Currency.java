package com.spiderbiggen.steampricetracker.util;

import java.util.Arrays;
import java.util.Locale;

public enum Currency {
    USD(1, "$", ""),
    GBP(2, "\u00a3", ""),
    EUR(3, "", "\u20ac"),
    CHF(4, "CHF ", ""),
    RUB(5, "", " p\u0443\u0431."), //pуб
    BRL(7, "R$ ", ""),
    JPY(8, "\u00a5 ", ""), //¥
    SEK(9, "", " kr"),
    IDR(10, "Rp 17 ", ""),
    MYR(11, "MR", ""),
    PHP(12, "P", ""), //₱ ?
    SGD(13, "S$", ""),
    THB(14, "\u0e3f", ""), //฿
    KRW(16, "\u20a9 ", ""), //₩
    TRY(17, "", " TL"), //₺ \u20ba
    MXN(19, "Mex$ ", ""),
    CAD(20, "CDN$ ", ""),
    NZD(22, "NZ$ ", ""),
    CNY(23, "\u00a5 ", ""), //¥
    INR(24, "\u20b9 ", ""),
    CLP(25, "CLP$ ", ""),
    PEN(26, "S/ ", ""),
    COP(27, "COL$ ", ""),
    ZAR(28, "R ", ""),
    HKD(29, "HK$ ", ""),
    TWD(30, "NT$ ", ""),
    SAR(31, "", " SR"),
    AED(32, "", " AED");

    private final int index;
    private final String prefix;
    private final String suffix;

    /**
     * Enum constructor.
     *
     * @param index  The id in the request url.
     * @param prefix The currency prefix.
     * @param suffix The currency suffix.
     */
    Currency(int index, String prefix, String suffix) {
        this.index = index;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public static Currency fromPrefixAndSuffix(String prefix, String suffix) {
        return Arrays.stream(Currency.values()).filter(c -> c.getPrefix().equals(prefix) && c.getSuffix().equals(suffix))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("No enum constant with " + prefix + " " + suffix));
    }

    /**
     * Create a nicely formatted currency String.
     *
     * @param number The number that needs to be formatted as a currency.
     * @return a complete string.
     */
    public String getValueString(Number number) {
        return String.format(Locale.ENGLISH, "%s%.2f%s", prefix, number.doubleValue(), suffix);
    }

    /**
     * Gets index
     *
     * @return value of index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Gets prefix
     *
     * @return value of prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Gets suffix
     *
     * @return value of suffix
     */
    public String getSuffix() {
        return suffix;
    }
}
