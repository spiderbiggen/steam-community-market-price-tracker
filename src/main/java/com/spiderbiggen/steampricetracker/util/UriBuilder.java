package com.spiderbiggen.steampricetracker.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UriBuilder {
    private List<String> path;
    private Map<String, String> queryParams, fragmentParams;
    private String scheme, authority;

    public UriBuilder() {
        path = new ArrayList<>();
        queryParams = new HashMap<>();
        fragmentParams = new HashMap<>();
    }

    public UriBuilder(String scheme, String authority) {
        this();
        this.scheme = scheme;
        this.authority = authority;
    }

    public UriBuilder scheme(String scheme) {
        this.scheme = scheme;
        return this;
    }

    public UriBuilder authority(String authority) {
        this.authority = authority;
        return this;
    }

    public UriBuilder appendPath(String path) {
        this.path.add(path);
        return this;
    }

    public UriBuilder appendQueryParameter(String parameter, String value) {
        queryParams.put(parameter, value);
        return this;
    }

    public UriBuilder appendQueryParameter(String parameter, Object value) {
        return appendQueryParameter(parameter, String.valueOf(value));
    }

    public UriBuilder appendFragmentParameter(String parameter, String value) {
        fragmentParams.put(parameter, value);
        return this;
    }

    public UriBuilder appendFragmentParameter(String parameter, Object value) {
        return appendFragmentParameter(parameter, String.valueOf(value));
    }

    public URI build() throws URISyntaxException {
        StringBuilder pathBuilder = new StringBuilder("/");
        StringBuilder queryParams = new StringBuilder();
        StringBuilder fragmentParams = new StringBuilder();
        this.path.forEach(p -> pathBuilder.append(p).append('/'));
        this.queryParams.forEach((key, value) -> queryParams.append(key).append('=').append(value).append('&'));
        int i = queryParams.lastIndexOf("&");
        if (i >= 0)
            queryParams.deleteCharAt(i);
        this.fragmentParams.forEach((key, value) -> fragmentParams.append(key).append('=').append(value).append('&'));
        i = fragmentParams.lastIndexOf("&");
        if (i >= 0)
            fragmentParams.deleteCharAt(i);
        String path = pathBuilder.length() <= 1 ? null : pathBuilder.toString();
        String query = queryParams.length() == 0 ? null : queryParams.toString();
        String fragment = fragmentParams.length() == 0 ? null : fragmentParams.toString();
        return new URI(scheme, authority, path, query, fragment);
    }

    public String buildString() throws URISyntaxException {
        return build().toString();
    }
}
