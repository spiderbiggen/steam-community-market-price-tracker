package com.spiderbiggen.steampricetracker.models;


import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Objects;

/**
 * Stores information about a game
 */
public class Game {

    private StringProperty appid;
    private StringProperty name;

    /**
     * Constructor.
     *
     * @param appid the appid of this game
     * @param name  the name of the game
     */
    public Game(String appid, String name) {
        this.appid = new SimpleStringProperty(appid);
        this.name = new SimpleStringProperty(name);
    }

    /**
     * JavaFX Property
     * @return appid property.
     */
    public StringProperty appidProperty() {
        return appid;
    }

    /**
     * Gets appid
     *
     * @return value of appid
     */
    public String getAppid() {
        return appid.getValue();
    }

    /**
     * Sets appid.
     *
     * @param appid the new value of appid
     */
    public void setAppid(String appid) {
        this.appid.setValue(appid);
    }

    /**
     * JavaFX Property
     * @return name property.
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * Gets name
     *
     * @return value of name
     */
    public String getName() {
        return name.getValue();
    }

    /**
     * Sets name.
     *
     * @param name the new value of name
     */
    public void setName(String name) {
        Platform.runLater(() -> this.name.setValue(name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAppid(), getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(getAppid(), game.getAppid()) &&
                Objects.equals(getName(), game.getName());
    }

    @Override
    public String toString() {
        return "Game{" +
                "appid='" + appid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
