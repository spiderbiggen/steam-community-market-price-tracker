package com.spiderbiggen.steampricetracker.models;


import com.spiderbiggen.steampricetracker.util.Currency;

import java.time.LocalDateTime;
import java.util.Objects;

public class History {

    private long id;
    private long itemid;
    private double price;
    private LocalDateTime date;
    private Currency currency;

    public History(LocalDateTime dateTime, double price, Currency currency) {
        this.date = dateTime;
        this.price = price;
        this.currency = currency;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getItemid() {
        return itemid;
    }

    public void setItemid(long itemid) {
        this.itemid = itemid;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setCurrency(String currency) {
        this.currency = Currency.valueOf(currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemid(), getPrice(), getDate(), getCurrency());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return getItemid() == history.getItemid() &&
                Double.compare(history.getPrice(), getPrice()) == 0 &&
                Objects.equals(getDate(), history.getDate()) &&
                getCurrency() == history.getCurrency();
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", itemid=" + itemid +
                ", price=" + price +
                ", date=" + date +
                ", currency=" + currency +
                '}';
    }
}
