package com.spiderbiggen.steampricetracker.models;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Objects;

public class Item implements Comparable<Item> {

    private long id;
    private StringProperty markethash;
    private StringProperty appid;

    public Item(long id, String appid, String markethash) {
        this(appid, markethash);
        this.id = id;
    }

    public Item(String appid, String markethash) {
        this.markethash = new SimpleStringProperty(markethash);
        this.appid = new SimpleStringProperty(appid);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getMarkethash() {
        return markethash.getValue();
    }

    public void setMarkethash(String markethash) {
        this.markethash.setValue(markethash);
    }

    public StringProperty markethashProperty() {
        return markethash;
    }

    public String getAppid() {
        return appid.getValue();
    }

    public void setAppid(String appid) {
        this.appid.setValue(appid);
    }

    public StringProperty appidProperty() {
        return appid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMarkethash(), getAppid());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(getMarkethash(), item.getMarkethash()) &&
                Objects.equals(getAppid(), item.getAppid());
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", markethash=\"" + markethash + '"' +
                ", appid='" + appid + '\'' +
                '}';
    }

    @Override
    public int compareTo(Item o) {
        return getAppid().compareTo(o.getAppid());
    }
}
