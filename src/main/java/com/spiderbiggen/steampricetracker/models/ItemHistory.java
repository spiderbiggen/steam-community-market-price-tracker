package com.spiderbiggen.steampricetracker.models;

import com.spiderbiggen.steampricetracker.util.Currency;

import java.time.LocalDateTime;
import java.util.Objects;

public class ItemHistory {

    private String game;
    private String marketHash;
    private Currency currency;
    private double lowestPrice;
    private double highestPrice;
    private double currentPrice;
    private LocalDateTime dateTime;

    public ItemHistory(String game, String marketHash, double lowestPrice, double highestPrice, double currentPrice, Currency currency, LocalDateTime dateTime) {
        this.game = game;
        this.marketHash = marketHash;
        this.lowestPrice = lowestPrice;
        this.highestPrice = highestPrice;
        this.currentPrice = currentPrice;
        this.currency = currency;
        this.dateTime = dateTime;
    }

    /**
     * Gets game
     *
     * @return value of game
     */
    public String getGame() {
        return game;
    }

    /**
     * Gets marketHash
     *
     * @return value of marketHash
     */
    public String getMarketHash() {
        return marketHash;
    }

    /**
     * Gets currency
     *
     * @return value of currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Gets lowestPrice
     *
     * @return value of lowestPrice
     */
    public double getLowestPrice() {
        return lowestPrice;
    }

    /**
     * Gets highestPrice
     *
     * @return value of highestPrice
     */
    public double getHighestPrice() {
        return highestPrice;
    }

    /**
     * Gets currentPrice
     *
     * @return value of currentPrice
     */
    public double getCurrentPrice() {
        return currentPrice;
    }

    /**
     * Gets lowestPrice surrounded by currency prefix and suffix
     *
     * @return value of lowestPrice
     */
    public String getLowestPriceString() {
        return currency.getValueString(lowestPrice);
    }

    /**
     * Gets highestPrice surrounded by currency prefix and suffix
     *
     * @return value of lowestPrice
     */
    public String getHighestPriceString() {
        return currency.getValueString(highestPrice);
    }

    /**
     * Gets currentPrice surrounded by currency prefix and suffix
     *
     * @return value of lowestPrice
     */
    public String getCurrentPriceString() {
        return currency.getValueString(currentPrice);
    }

    /**
     * Gets dateTime
     *
     * @return value of dateTime
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGame(), getMarketHash(), getCurrency(), getLowestPrice(), getHighestPrice(), getCurrentPrice(), getDateTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemHistory that = (ItemHistory) o;
        return Double.compare(that.getLowestPrice(), getLowestPrice()) == 0 &&
                Double.compare(that.getHighestPrice(), getHighestPrice()) == 0 &&
                Double.compare(that.getCurrentPrice(), getCurrentPrice()) == 0 &&
                Objects.equals(getGame(), that.getGame()) &&
                Objects.equals(getMarketHash(), that.getMarketHash()) &&
                getCurrency() == that.getCurrency() &&
                Objects.equals(getDateTime(), that.getDateTime());
    }

    @Override
    public String toString() {
        return "ItemHistory{" +
                "game='" + game + '\'' +
                ", marketHash=\"" + marketHash + '"' +
                ", currency=" + currency +
                ", lowestPrice=" + lowestPrice +
                ", highestPrice=" + highestPrice +
                ", currentPrice=" + currentPrice +
                ", dateTime=" + dateTime +
                '}';
    }
}
