# Steam Community Market Price Tracker

This Java project allows you to follow multiple items on the steam community market and display them
in a graph.

-------
[![pipeline status](https://gitlab.com/spiderbiggen/steam-community-market-price-tracker/badges/master/pipeline.svg)](https://gitlab.com/spiderbiggen/steam-community-market-price-tracker/commits/master)
[![coverage report](https://gitlab.com/spiderbiggen/steam-community-market-price-tracker/badges/master/coverage.svg)](https://gitlab.com/spiderbiggen/steam-community-market-price-tracker/commits/master)